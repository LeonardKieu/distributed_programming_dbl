db.getCollection('cacChiTietChamCong').aggregate([
    { '$match': { '_id.maNhanVien': '0003' } },
    {
        '$project': {
            '_id.ngayChamCong': {
                '$dateFromString': {
                    'dateString': '$_id.ngayChamCong'
                }
            },
            '_id.maNhanVien': 1,
            'daChamCong': 1
        }
    },
    {
        '$project': {
            'month': { '$month': "$_id.ngayChamCong" },
            'year': { '$year': "$_id.ngayChamCong" },
            'daChamCong': 1
        }
    },
    { '$match': { 'month': 10, 'year': 2019 } },
    {
        '$project': {
            '_id.maNhanVien': 1,
            'daChamCong': 1,
            '_id.ngayChamCong': { '$dateToString': { 'format': "%Y-%m-%d", date: "$_id.ngayChamCong" } }
        }
    }
])      