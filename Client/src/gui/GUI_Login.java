package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.rmi.Naming;
import java.rmi.RemoteException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import entities.NhanVien;
import services.ChiTietChamCongServices;
import services.NhanVienServices;

public class GUI_Login extends JFrame implements ActionListener {

	private JButton btnDangNhap;
	private JTextField txtUsername;
	private JPasswordField txtPassword;
	private SecurityManager securityManager;
	private NhanVienServices nvService;
	public static NhanVien nhanVien = new NhanVien();
	public static String ipAddress = "";

	public GUI_Login() {
		setTitle("Đăng nhập");
		setSize(400, 250);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(".\\icon_libs\\logo.png"));
		addGUI();
		try {
			SecurityManager securityManager = System.getSecurityManager();
			if (securityManager == null) {
				System.setProperty("java.security.policy", "policy\\policyFile.policy");
				securityManager = new SecurityManager();
			}
			nvService = (NhanVienServices) Naming.lookup("rmi://" + GUI_Login.ipAddress + ":1999/nv");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void addGUI() {
		
		if (GUI_Login.ipAddress.equals("")) {
			GUI_Login.ipAddress = JOptionPane.showInputDialog(this, "Nhập địa chỉ IP Server").trim();
		}

		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

		/**
		 * Đoạn này khai báo các panel (container) cho các phần tử trong giao diện đăng
		 * nhập Lần lượt là: tiêu đề, username, password, nút đăng nhập,...
		 */

		JPanel panelForTitle, panelForUsername, panelForPassword, panelForLoginButton;

		panelForTitle = new JPanel();
//		panelForTitle.setLayout(new BoxLayout(panelForTitle, BoxLayout.X_AXIS));
		panelForTitle.setLayout(new FlowLayout());

		panelForUsername = new JPanel();
		panelForUsername.setLayout(new BoxLayout(panelForUsername, BoxLayout.X_AXIS));

		panelForPassword = new JPanel();
		panelForPassword.setLayout(new BoxLayout(panelForPassword, BoxLayout.X_AXIS));

		panelForLoginButton = new JPanel();
		panelForLoginButton.setLayout(new FlowLayout());

		add(panelForTitle);
		add(panelForUsername);
		add(panelForPassword);
		add(panelForLoginButton);

		JLabel labelTitle = new JLabel("ĐĂNG NHẬP");
		labelTitle.setFont(new Font("Roboto", Font.BOLD, 20));
		labelTitle.setForeground(new Color(70, 130, 180));

		panelForTitle.add(labelTitle);
		panelForTitle.setBackground(new Color(255, 165, 0));

		try {
			BufferedImage bufferedImage = ImageIO.read(GUI_Login.class.getResource("/icon_libs/user-icon.png"));
			JLabel labelUsername = new JLabel(new ImageIcon(bufferedImage));
			panelForUsername.add(labelUsername);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		/**
		 * Đoạn này để tạo border radius cho textfield username
		 */
		txtUsername = new JTextField() {
			@Override
			protected void paintComponent(Graphics g) {
				if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
					Graphics2D g2 = (Graphics2D) g.create();
					g2.setPaint(getBackground());
					g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(0, 0, getWidth() - 1, getHeight() - 1));
					g2.dispose();
				}
				super.paintComponent(g);
			}

			@Override
			public void updateUI() {
				super.updateUI();
				setOpaque(false);
				setBorder(new RoundedCornerBorder());
			}
		};
		panelForUsername.add(txtUsername);
		txtUsername.setMaximumSize(new Dimension(250, 30));
		
		try {
			BufferedImage bufferedImage = ImageIO.read(GUI_Login.class.getResource("/icon_libs/password-icon.png"));
			JLabel labelPassword = new JLabel(new ImageIcon(bufferedImage));
			panelForPassword.add(labelPassword);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		/**
		 * Đoạn này để tạo border radius cho textfield password...
		 */
		txtPassword = new JPasswordField() {
			@Override
			protected void paintComponent(Graphics g) {
				if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
					Graphics2D g2 = (Graphics2D) g.create();
					g2.setPaint(getBackground());
					g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(0, 0, getWidth() - 1, getHeight() - 1));
					g2.dispose();
				}
				super.paintComponent(g);
			}

			@Override
			public void updateUI() {
				super.updateUI();
				setOpaque(false);
				setBorder(new RoundedCornerBorder());
			}
		};
		panelForPassword.add(txtPassword);
		txtPassword.setMaximumSize(new Dimension(250, 30));

		btnDangNhap = new JButton("Đăng nhập");
		panelForLoginButton.add(btnDangNhap);

		btnDangNhap.addActionListener(this);
	}

	public static void main(String[] args) {
		new GUI_Login().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		if (o.equals(btnDangNhap)) {
			String idNhanVien = txtUsername.getText();
			String matKhau = String.valueOf(txtPassword.getPassword());
			try {
				if (nvService.dangNhap(idNhanVien, matKhau)) {
					nhanVien = nvService.getThongTinNhanVien(idNhanVien);
					if (nhanVien.getChucVu() == 1) {
						new GUI_GiaoDienChamCong(null).setVisible(true);
						dispose();
					} else {
						new GUI_QuanLyNhanVien().setVisible(true);
						dispose();
					}
				} else {
					JOptionPane.showMessageDialog(this, "Mật khẩu hoặc tên đăng nhập không đúng!");
				}
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

}
