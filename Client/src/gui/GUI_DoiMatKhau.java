package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.Naming;
import java.rmi.RemoteException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import services.NhanVienServices;

public class GUI_DoiMatKhau extends JFrame implements ActionListener {
	private JPanel title = new JPanel();
	private JPanel left = new JPanel();
	private JPanel right = new JPanel();
	private JPanel bot = new JPanel();
	private JLabel doiMK;
	private JLabel label1, label2, label3, label4, xacThuc1, xacThuc2, xacThuc3, xacThuc4;
	private JTextField txtIdNhanVien;
	private JPasswordField txtMatKhauHienTai, txtMatKhauMoi, txtXacNhanMatKhauMoi;
	private JButton btnXacNhan;
	private SecurityManager securityManager;
	private NhanVienServices nvService;

	public GUI_DoiMatKhau() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(".\\icon_libs\\logo.png"));
		setSize(600, 300);
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("Đổi Mật Khẩu");
		try {
			SecurityManager securityManager = System.getSecurityManager();
			if (securityManager == null) {
				System.setProperty("java.security.policy", "policy\\policyFile.policy");
				securityManager = new SecurityManager();
			}
			nvService = (NhanVienServices) Naming.lookup("rmi://" + GUI_Login.ipAddress + ":1999/nv");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		// Tao title
		title.setPreferredSize(new Dimension(600, 50));
		title.setBackground(new Color(255, 165, 0));
		title.add(Box.createVerticalStrut(40));

		title.add(doiMK = new JLabel("Đổi Mật Khẩu"), BorderLayout.CENTER);
		doiMK.setFont(new Font("Roboto", Font.BOLD, 20));
		doiMK.setForeground(new Color(70, 130, 180));
		// Tao Panel chua cai text va khung
		JPanel t1 = new JPanel();
		t1.setLayout(new BoxLayout(t1, BoxLayout.X_AXIS));
		JPanel t2 = new JPanel();
		t2.setLayout(new BoxLayout(t2, BoxLayout.X_AXIS));
		JPanel t3 = new JPanel();
		t3.setLayout(new BoxLayout(t3, BoxLayout.X_AXIS));
		JPanel t4 = new JPanel();
		t4.setLayout(new BoxLayout(t4, BoxLayout.X_AXIS));
		JPanel t5 = new JPanel();
		t5.setLayout(new BoxLayout(t5, BoxLayout.X_AXIS));

		t1.add(label1 = new JLabel("ID Nhân Viên"));
		t1.add(txtIdNhanVien = new JTextField());
		txtIdNhanVien.setEnabled(false);
		t1.add(Box.createHorizontalStrut(15));
		t1.add(xacThuc1 = new JLabel());

		t2.add(label2 = new JLabel("Mật Khẩu Hiện Tại"));
		t2.add(txtMatKhauHienTai = new JPasswordField());
		t2.add(Box.createHorizontalStrut(15));
		t2.add(xacThuc2 = new JLabel());

		t3.add(label3 = new JLabel("Mật Khẩu Mới"));
		t3.add(txtMatKhauMoi = new JPasswordField());
		t3.add(Box.createHorizontalStrut(15));
		t3.add(xacThuc3 = new JLabel());

		t4.add(label4 = new JLabel("Xác Nhận Mật Khẩu Mới    "));
		t4.add(txtXacNhanMatKhauMoi = new JPasswordField());
		t4.add(Box.createHorizontalStrut(15));
		t4.add(xacThuc4 = new JLabel());

		// Set kích thước label
		label1.setMaximumSize(label4.getPreferredSize());
		label2.setMaximumSize(label4.getPreferredSize());
		label3.setMaximumSize(label4.getPreferredSize());

		txtIdNhanVien.setMaximumSize(new Dimension(300, 25));
		txtMatKhauHienTai.setMaximumSize(new Dimension(300, 25));
		txtMatKhauMoi.setMaximumSize(new Dimension(300, 25));
		txtXacNhanMatKhauMoi.setMaximumSize(new Dimension(300, 25));

		xacThuc1.setFont(new Font("Roboto", Font.ITALIC, 12));
		xacThuc1.setForeground(Color.red);
		xacThuc2.setFont(new Font("Roboto", Font.ITALIC, 12));
		xacThuc2.setForeground(Color.red);
		xacThuc3.setFont(new Font("Roboto", Font.ITALIC, 12));
		xacThuc3.setForeground(Color.red);
		xacThuc4.setFont(new Font("Roboto", Font.ITALIC, 12));
		xacThuc4.setForeground(Color.red);

		xacThuc1.setMaximumSize(new Dimension(50, 25));
		xacThuc2.setMaximumSize(new Dimension(50, 25));
		xacThuc3.setMaximumSize(new Dimension(50, 25));
		xacThuc4.setMaximumSize(new Dimension(50, 25));

		left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));

		left.add(Box.createVerticalStrut(15));
		left.add(t1);
		left.add(t2);
		left.add(t3);
		left.add(t4);

		bot.setLayout(new BoxLayout(bot, BoxLayout.X_AXIS));

		bot.add(btnXacNhan = new JButton("Xác Nhận"));

		left.add(bot);
		add(title, BorderLayout.NORTH);
		add(left, BorderLayout.CENTER);

		btnXacNhan.addActionListener(this);
		txtIdNhanVien.setText(GUI_Login.nhanVien.getMaNV());

		setVisible(true);
	}

	public boolean doiMatKhau() throws RemoteException {
		String matKhauHienTai = String.valueOf(txtMatKhauHienTai.getPassword());
		String matKhauMoi = String.valueOf(txtMatKhauMoi.getPassword());
		String matKhauXacThuc = String.valueOf(txtXacNhanMatKhauMoi.getPassword());

		if (txtMatKhauMoi.getPassword().length < 8) {
			JOptionPane.showMessageDialog(this, "Mật khẩu phải có ít nhất 8 kí tự!");
			txtMatKhauMoi.requestFocus();
			return false;
		}

		if (!matKhauMoi.equals(matKhauXacThuc) || matKhauMoi.equals("") || matKhauXacThuc.equals("")) {
			JOptionPane.showMessageDialog(this, "Xác thực mật khẩu không chính xác!");
			txtXacNhanMatKhauMoi.requestFocus();
			return false;
		}
		if (nvService.resetPassword(GUI_Login.nhanVien.getMaNV(), matKhauHienTai, matKhauMoi)) {
			return true;
		}
		JOptionPane.showMessageDialog(this, "Mật khẩu cũ không chính xác!");
		txtMatKhauHienTai.requestFocus();
		return false;
	}

	public static void main(String[] args) {
		new GUI_Login().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		if (o.equals(btnXacNhan)) {
			try {
				if (doiMatKhau()) {
					int decision = JOptionPane.showConfirmDialog(this, "Thành công!", "", JOptionPane.CLOSED_OPTION,
							JOptionPane.INFORMATION_MESSAGE);

					if (decision == JOptionPane.OK_OPTION) {
						dispose();
					}
				}
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

}
