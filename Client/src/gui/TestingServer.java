package gui;

import java.rmi.Naming;
import java.time.LocalDate;

import entities.NhanVien;
import services.ChiTietChamCongServices;
import services.NhanVienServices;

public class TestingServer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			SecurityManager securityManager = System.getSecurityManager();
			if (securityManager == null) {
				System.setProperty("java.security.policy", "policy\\policyFile.policy");
				securityManager = new SecurityManager();
			}
			ChiTietChamCongServices ctccService = (ChiTietChamCongServices) Naming.lookup("rmi://localhost:1999/ctcc");
			System.out.println(ctccService.getNgayNghiKoHopLe("0001", 10, 2019));
			System.out.println(ctccService.getNgayNghiNhungLam("0001", 10, 2019));
			System.out.println("OK");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
