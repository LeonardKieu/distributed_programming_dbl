package gui;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import dao.CalendarCalc;
import dao.Holiday;

public class Judger {
	/**
	 * Ngày bth: 1, Lễ: 0, T7, CN: -1
	 * @param solarToday
	 * @return
	 */
	public static int judgeLoaiNgay(LocalDate solarToday) {
//		Detect loaiNgay of today
		List<Holiday> listSolarHoliday = Arrays.asList(new Holiday[] {
				new Holiday(1, 1), new Holiday(30, 4), new Holiday(1, 5), new Holiday(2, 9)
		});
		List<Holiday> listLunarHoliday = Arrays.asList(new Holiday[] {
				new Holiday(1, 1), new Holiday(2, 1), new Holiday(3, 1), new Holiday(4, 1), new Holiday(5, 1),
				new Holiday(10, 3), 
		});
//		Calc lunarToday
		int[] lunarTodayArr = CalendarCalc.Solar2Lunar(solarToday.getDayOfMonth(), solarToday.getMonthValue(), solarToday.getYear());
		LocalDate lunarToday = LocalDate.of(lunarTodayArr[2], lunarTodayArr[1], lunarTodayArr[0]);
		
		if(listSolarHoliday.contains(new Holiday(solarToday.getDayOfMonth(), solarToday.getMonthValue())) || listLunarHoliday.contains(new Holiday(lunarToday.getDayOfMonth(), lunarToday.getMonthValue())))
//			Holiday 0
			return 0;
		else if(solarToday.getDayOfWeek().equals(DayOfWeek.SATURDAY) || solarToday.getDayOfWeek().equals(DayOfWeek.SUNDAY))
//			Weekends -1
			return -1;
		else
//			Weekday 1
			return 1;
	}
	
	public static void main(String[] args) {
		System.out.println(Judger.judgeLoaiNgay(LocalDate.of(2019, 5, 1)));
	}
}
