package gui;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class Testing {

	public static void main(String[] args) {
		EntityManager em = Persistence.createEntityManagerFactory("dp_large_exercise").createEntityManager();
		System.err.println("OK");
		new GUI_Login().setVisible(true);
	}

}
