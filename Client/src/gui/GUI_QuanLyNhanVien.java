package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;

import entities.NhanVien;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import services.ChiTietChamCongServices;
import services.NhanVienServices;

public class GUI_QuanLyNhanVien extends JFrame implements ActionListener, MouseListener {

	private JComboBox<String> cbbPhongBan;
	private JButton btnXemCuThe, btnTaoTaiKhoan, btnXoaTaiKhoan, btnQuanLyThongTinCaNhan, btnTimKiem, btnDangXuat;
	private JTextField txtTimKiem, txtHoTen, txtEmail, txtSoDienThoai, txtSoNha, txtTenDuong, txtTenPhuongXa,
			txtTenQuanHuyen, txtTenTinh, txtNgaySinh;
	private DefaultTableModel tableModelDanhSachNhanVien;
	private JTable tableDanhSachNhanVien;
	private JScrollPane scrollPaneDanhSachNhanVien;
	private JLabel labelChucVu, labelMaNhanVien;
	private UtilDateModel model;
	private JDatePanelImpl datePanel;
	private JDatePickerImpl datePicker;
	private HashMap<String, String> danhSachPhongBan = new HashMap<String, String>();
	private final ArrayList<String> danhSachMaPhongBan = new ArrayList<String>(
			Arrays.asList(new String[] { "A1", "A2", "A3", "A4", "A5" }));
	private NhanVienServices nhanVienServices;
	private ChiTietChamCongServices chiTietChamCongServices;
	private List<NhanVien> nhanViens;
	private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	private DecimalFormat decimalFormat = new DecimalFormat("#,### VNĐ");
	private int flag = -1;
	private GUI_TaoTaiKhoanNhanVien gui_TaoTk;
	private GUI_ThongTinCaNhan gui_qlThongTin;
	private GUI_GiaoDienChamCong gui_xemChamCong;

	public GUI_QuanLyNhanVien() {
		setTitle("Quản lý nhân viên");
		setSize(1200, 700);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(".\\icon_libs\\logo.png"));
		try {
			SecurityManager securityManager = System.getSecurityManager();
			if (securityManager == null) {
				System.setProperty("java.security.policy", "policy\\policyFile.policy");
				securityManager = new SecurityManager();
			}
			nhanVienServices = (NhanVienServices) Naming.lookup("rmi://" + GUI_Login.ipAddress + ":1999/nv");
			chiTietChamCongServices = (ChiTietChamCongServices) Naming
					.lookup("rmi://" + GUI_Login.ipAddress + ":1999/ctcc");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		addUI();
	}

	public void addUI() {
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		/**
		 * 
		 */
		JPanel panelTop, panelCenter;
		panelTop = new JPanel();
		panelTop.setLayout(new FlowLayout(FlowLayout.CENTER));
		panelCenter = new JPanel();
		panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.Y_AXIS));

		add(panelTop);
		add(panelCenter);
		/**
		 * Đoạn code này setup các button điều khiển ở phần header của giao diện
		 */
		cbbPhongBan = new JComboBox<String>();
		panelTop.add(cbbPhongBan);

		btnXemCuThe = new JButton("Xem cụ thể");
		try {
			BufferedImage bufferedImage = ImageIO.read(GUI_Login.class.getResource("/icon_libs/detail-icon.png"));
			btnXemCuThe = new JButton("Xem cụ thể", new ImageIcon(bufferedImage));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		panelTop.add(btnXemCuThe);
		cbbPhongBan.setPreferredSize(btnXemCuThe.getPreferredSize());

		txtTimKiem = new JTextField(15);
		panelTop.add(txtTimKiem);
		txtTimKiem.setPreferredSize(
				new Dimension(txtTimKiem.getPreferredSize().width, btnXemCuThe.getPreferredSize().height));

		btnTimKiem = new JButton(new ImageIcon("/icon_libs/search-icon.png"));
		try {
			BufferedImage bufferedImage = ImageIO.read(GUI_Login.class.getResource("/icon_libs/search-icon.png"));
			btnTimKiem = new JButton(new ImageIcon(bufferedImage));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		panelTop.add(btnTimKiem);

		btnTaoTaiKhoan = new JButton("Tạo tài khoản");
		try {
			BufferedImage bufferedImage = ImageIO.read(GUI_Login.class.getResource("/icon_libs/create-icon.png"));
			btnTaoTaiKhoan = new JButton("Tạo tài khoản", new ImageIcon(bufferedImage));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		panelTop.add(btnTaoTaiKhoan);

		btnXoaTaiKhoan = new JButton("Xóa tài khoản");
		try {
			BufferedImage bufferedImage = ImageIO.read(GUI_Login.class.getResource("/icon_libs/remove-icon.png"));
			btnXoaTaiKhoan = new JButton("Xóa tài khoản", new ImageIcon(bufferedImage));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		panelTop.add(btnXoaTaiKhoan);

		btnQuanLyThongTinCaNhan = new JButton("Quản lý thông tin cá nhân");
		try {
			BufferedImage bufferedImage = ImageIO.read(GUI_Login.class.getResource("/icon_libs/edit-icon.png"));
			btnQuanLyThongTinCaNhan = new JButton("Quản lý thông tin cá nhân", new ImageIcon(bufferedImage));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		panelTop.add(btnQuanLyThongTinCaNhan);

		btnDangXuat = new JButton("Đăng xuất");
		try {
			BufferedImage bufferedImage = ImageIO.read(GUI_Login.class.getResource("/icon_libs/log-out-icon.png"));
			btnDangXuat = new JButton("Đăng xuất", new ImageIcon(bufferedImage));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		panelTop.add(btnDangXuat);
		/**
		 * đoạn code này setup phần panel cho danh sách nhân sự của phòng ban
		 */
		JPanel panelInfoEmployee = new JPanel();
		panelInfoEmployee.setLayout(new BoxLayout(panelInfoEmployee, BoxLayout.X_AXIS));
		panelCenter.add(panelInfoEmployee);
		panelInfoEmployee.setBorder(BorderFactory.createTitledBorder("Thông tin nhân sự"));
		/**
		 * đoạn code này setup phần table danh sách nhân sự của phòng ban
		 */
		JPanel panelForListEmployee = new JPanel();
		panelCenter.add(panelForListEmployee);
		String[] header = { "Mã nhân viên", "Họ tên", "Chức vụ", "Ngày sinh", /* "Giới tính", */ "Email",
				"Số ngày chấm công", "Tiền lương" };
		tableModelDanhSachNhanVien = new DefaultTableModel(header, 0);
		tableDanhSachNhanVien = new JTable(tableModelDanhSachNhanVien) {
			public boolean isCellEditable(int row, int col) {
				return false;
			}

			public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
				Component c = super.prepareRenderer(renderer, row, col);
				if (row % 2 == 0 && !isCellSelected(row, col)) {
					c.setBackground(Color.decode("#F1F1F1"));
				} else if (!isCellSelected(row, col)) {
					c.setBackground(Color.decode("#D7F1FF"));
				} else {
					c.setBackground(Color.decode("#25C883"));
				}
				return c;
			}
		};

		JTableHeader header1 = tableDanhSachNhanVien.getTableHeader();
		header1.setBackground(Color.darkGray);
		header1.setForeground(Color.white);
		header1.setOpaque(false);
		// xét cứng cột
		tableDanhSachNhanVien.getTableHeader().setReorderingAllowed(false);

		scrollPaneDanhSachNhanVien = new JScrollPane(tableDanhSachNhanVien);
		panelForListEmployee.add(scrollPaneDanhSachNhanVien);
		scrollPaneDanhSachNhanVien.setPreferredSize(new Dimension(1150, 280));
		panelForListEmployee.setBorder(BorderFactory.createTitledBorder("Danh sách nhân viên"));
		/**
		 * đoạn code này setup phần hình ảnh đại diện của nhân viên và chức vụ của nhân
		 * viên đó
		 */

		// panel này dùng để chứa hình ảnh
		JPanel panelForImageEmployee = new JPanel();
		panelForImageEmployee.setLayout(null);

		// label này chứa ảnh đại diện của nhân viên
		JLabel labelImageEmployee = new JLabel(new ImageIcon("icon_libs/userBigmap-icon.png"));
		try {
			BufferedImage bufferedImage = ImageIO.read(GUI_Login.class.getResource("/icon_libs/userBigmap-icon.png"));
			labelImageEmployee = new JLabel(new ImageIcon(bufferedImage));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		labelImageEmployee.setBounds(50, 0, 200, 200);
		panelForImageEmployee.add(labelImageEmployee);

		// label này hiển thị chức vụ của user đó, sẽ dynamic theo kết quả trả về của
		// api
		labelChucVu = new JLabel("");
		labelChucVu.setBounds(100, 205, 100, 20);
		panelForImageEmployee.add(labelChucVu);
		panelInfoEmployee.add(panelForImageEmployee);

		/**
		 * panel này chứa thông tin của nhân viên
		 */
		JPanel panelForDetailInfo = new JPanel();
		panelForDetailInfo.setLayout(new GridLayout(5, 2, 5, 5));
		panelInfoEmployee.add(panelForDetailInfo);
		/**
		 * Khởi tạo các textfield chứa thông tin nhân viên
		 */
		labelMaNhanVien = new JLabel("");
		txtHoTen = new JTextField(20);
		txtEmail = new JTextField(20);
		txtSoDienThoai = new JTextField(20);
		txtSoNha = new JTextField(20);
		txtTenDuong = new JTextField(20);
		txtTenPhuongXa = new JTextField(20);
		txtTenQuanHuyen = new JTextField(20);
		txtTenTinh = new JTextField(20);

		model = new UtilDateModel();
		datePanel = new JDatePanelImpl(model);
		datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
		/**
		 * Khởi tạo các label tiêu đề chú thích cho phần thông tin của nhân viên
		 */
		JLabel labelTitleMaNV = new JLabel("Mã nhân viên:");
		JLabel labelHoTen = new JLabel("Họ tên:");
		JLabel labelEmail = new JLabel("Email:");
		JLabel labelSoDienThoai = new JLabel("Số điện thoại:");
		JLabel labelSoNha = new JLabel("Số nhà:");
		JLabel labelTenDuong = new JLabel("Tên đường:");
		JLabel labelTenPhuongXa = new JLabel("Tên phường xã:");
		JLabel labelTenQuanHuyen = new JLabel("Tên quận huyện:");
		JLabel labelTenTinh = new JLabel("Tên tỉnh:");
		JLabel labelNgaySinh = new JLabel("Ngày sinh");
		/**
		 * Khởi tạo các panel chứa thông tin của nhân viên, mỗi trường thông tin là 1
		 * panel
		 */
		JPanel panelMaNhanVien = new JPanel();
		panelMaNhanVien.setLayout(new FlowLayout(FlowLayout.RIGHT));
		panelMaNhanVien.add(labelTitleMaNV);
		panelMaNhanVien.add(labelMaNhanVien);
		panelForDetailInfo.add(panelMaNhanVien);

		List<JTextField> textFields = Arrays.asList(new JTextField[] { txtHoTen, txtEmail, txtSoDienThoai, txtSoNha,
				txtTenDuong, txtTenPhuongXa, txtTenQuanHuyen, txtTenTinh });
		List<JLabel> labels = Arrays.asList(new JLabel[] { labelHoTen, labelEmail, labelSoDienThoai, labelSoNha,
				labelTenDuong, labelTenPhuongXa, labelTenQuanHuyen, labelTenTinh });
		JPanel panelHoTen = new JPanel();
		JPanel panelEmail = new JPanel();
		JPanel panelSoDienThoai = new JPanel();
		JPanel panelSoNha = new JPanel();
		JPanel panelTenDuong = new JPanel();
		JPanel panelTenPhuongXa = new JPanel();
		JPanel panelTenQuanHuyen = new JPanel();
		JPanel panelTenTinh = new JPanel();
		/**
		 * add các panel vào panelForDetailInfo (panel chứa thông tin nhân viên)
		 */
		List<JPanel> panels = Arrays.asList(new JPanel[] { panelHoTen, panelEmail, panelSoDienThoai, panelSoNha,
				panelTenDuong, panelTenPhuongXa, panelTenQuanHuyen, panelTenTinh });
		for (int i = 0; i < panels.size(); i++) {
			panels.get(i).setLayout(new FlowLayout(FlowLayout.RIGHT));
			panels.get(i).add(labels.get(i));
			panels.get(i).add(textFields.get(i));
			panelForDetailInfo.add(panels.get(i));
		}

		JPanel panelNgaySinh = new JPanel();
		panelNgaySinh.setLayout(new FlowLayout(FlowLayout.RIGHT));
		panelNgaySinh.add(labelNgaySinh);
		panelNgaySinh.add(datePicker);
		panelForDetailInfo.add(panelNgaySinh);

		labelMaNhanVien.setPreferredSize(txtHoTen.getPreferredSize());

		if (GUI_Login.nhanVien.getChucVu() == 2) {

			danhSachPhongBan.put(GUI_Login.nhanVien.getPhongBan().getMaPhongBan(),
					GUI_Login.nhanVien.getPhongBan().getTenPhongBan());
			cbbPhongBan.addItem(GUI_Login.nhanVien.getPhongBan().getTenPhongBan());
		} else {
			danhSachPhongBan.put("A1", "Phong Ke Toan");
			danhSachPhongBan.put("A2", "Phong Kinh Doanh");
			danhSachPhongBan.put("A3", "Phong An Ninh");
			danhSachPhongBan.put("A4", "Phong Quan Ly");
			danhSachPhongBan.put("A5", "Phong Nhan Cong");
			for (String maPB : danhSachMaPhongBan) {
				cbbPhongBan.addItem(danhSachPhongBan.get(maPB));
			}
		}

		if (GUI_Login.nhanVien.getChucVu() == 2) {
			getNhanVienTheoPhongBan(GUI_Login.nhanVien.getPhongBan().getMaPhongBan(), "");
		} else if (GUI_Login.nhanVien.getChucVu() == 3) {
			getNhanVienTheoPhongBan(danhSachMaPhongBan.get(cbbPhongBan.getSelectedIndex()), "");
		}
		napDanhSachNhanVien();

		// Khóa textfile thông tin của nhân viên
		txtEmail.setEnabled(false);
		txtEmail.setDisabledTextColor(Color.black);

		txtHoTen.setEnabled(false);
		txtHoTen.setDisabledTextColor(Color.black);

		txtSoDienThoai.setEnabled(false);
		txtSoDienThoai.setDisabledTextColor(Color.black);

		txtSoNha.setEnabled(false);
		txtSoNha.setDisabledTextColor(Color.black);

		txtTenDuong.setEnabled(false);
		txtTenDuong.setDisabledTextColor(Color.black);

		txtTenPhuongXa.setEnabled(false);
		txtTenPhuongXa.setDisabledTextColor(Color.black);

		txtTenQuanHuyen.setEnabled(false);
		txtTenQuanHuyen.setDisabledTextColor(Color.black);

		txtTenTinh.setEnabled(false);
		txtTenTinh.setDisabledTextColor(Color.black);

		datePicker.getComponent(1).setEnabled(false);

		/**
		 * 
		 */

		cbbPhongBan.addActionListener(this);
		btnXemCuThe.addActionListener(this);
		btnTaoTaiKhoan.addActionListener(this);
		btnXoaTaiKhoan.addActionListener(this);
		btnQuanLyThongTinCaNhan.addActionListener(this);
		btnTimKiem.addActionListener(this);
		btnDangXuat.addActionListener(this);
		tableDanhSachNhanVien.addMouseListener(this);
	}

	public void getNhanVienTheoPhongBan(String maPB, String tieuChi) {
		try {
			if (!tieuChi.equalsIgnoreCase("")) {
				nhanViens = nhanVienServices.getDanhSachNhanVien(maPB, tieuChi);
			} else {
				nhanViens = nhanVienServices.getDanhSachNhanVien(maPB);
			}
//			System.out.println(nhanVienServices.getDanhSachNhanVien(maPB, " Le Nguyen").size());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void napDanhSachNhanVien() {
		tableModelDanhSachNhanVien.setRowCount(0);
		for (NhanVien nhanVien : nhanViens) {
			String chucVu = "";
			if (nhanVien.getChucVu() == 1) {
				chucVu = "Nhân viên";
			} else if (nhanVien.getChucVu() == 2) {
				chucVu = "Trưởng phòng";
			} else {
				chucVu = "Quản lí";
			}
			int soNgayChamCong = 0;
			try {
				soNgayChamCong = chiTietChamCongServices.getDanhSachChiTietChamCong(nhanVien.getMaNV(),
						LocalDate.now().getMonthValue(), LocalDate.now().getYear()).size();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String[] dataOfRow = { nhanVien.getMaNV(), nhanVien.getHoTen(), chucVu,
					nhanVien.getNgaySinh().format(dateTimeFormatter), nhanVien.getEmail(),
					Integer.toString(soNgayChamCong), decimalFormat.format(nhanVien.getLuongTheoThang()) };
			tableModelDanhSachNhanVien.addRow(dataOfRow);
		}
	}

	public void hienThiThongTinLenTextField(int index) {
		NhanVien nhanVien = nhanViens.get(index);
		String chucVu = "";
		if (nhanVien.getChucVu() == 1) {
			chucVu = "Nhân viên";
		} else if (nhanVien.getChucVu() == 2) {
			chucVu = "Trưởng phòng";
		} else {
			chucVu = "Quản lí";
		}
		labelChucVu.setText(chucVu);
		labelMaNhanVien.setText(nhanVien.getMaNV());
		txtEmail.setText(nhanVien.getEmail());
		txtSoNha.setText(nhanVien.getDiaChi().getSoNha());
		txtTenPhuongXa.setText(nhanVien.getDiaChi().getTenPhuongXa());
		txtTenTinh.setText(nhanVien.getDiaChi().getTenTinhTP());
		txtTenQuanHuyen.setText(nhanVien.getDiaChi().getTenQuanHuyen());
		txtHoTen.setText(nhanVien.getHoTen());
		txtSoDienThoai.setText(nhanVien.getSdt());
		txtTenDuong.setText(nhanVien.getDiaChi().getTenDuong());
		model.setDate(nhanVien.getNgaySinh().getYear(), nhanVien.getNgaySinh().getMonthValue() - 1,
				nhanVien.getNgaySinh().getDayOfMonth());
		model.setSelected(true);
	}

	public List<NhanVien> getNhanViens() {
		return nhanViens;
	}

	public static void main(String[] args) {
		new GUI_Login().setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		if (o.equals(cbbPhongBan)) {
			if (GUI_Login.nhanVien.getChucVu() == 2) {
				getNhanVienTheoPhongBan(GUI_Login.nhanVien.getPhongBan().getMaPhongBan(), "");
			} else if (GUI_Login.nhanVien.getChucVu() == 3) {
				getNhanVienTheoPhongBan(danhSachMaPhongBan.get(cbbPhongBan.getSelectedIndex()), "");
			}
			napDanhSachNhanVien();
		}
		if (o.equals(btnDangXuat)) {
			int choice = JOptionPane.showConfirmDialog(this, "Bạn có muốn đăng xuất?", "Xác nhận",
					JOptionPane.YES_NO_OPTION);
			if (choice == JOptionPane.YES_OPTION) {
				new GUI_Login().setVisible(true);
				dispose();
			}
		}
		if (o.equals(btnTimKiem)) {
			if (GUI_Login.nhanVien.getChucVu() == 2) {
				getNhanVienTheoPhongBan(GUI_Login.nhanVien.getPhongBan().getMaPhongBan(), txtTimKiem.getText());
			} else if (GUI_Login.nhanVien.getChucVu() == 3) {
				getNhanVienTheoPhongBan(danhSachMaPhongBan.get(cbbPhongBan.getSelectedIndex()), txtTimKiem.getText());
			}
			napDanhSachNhanVien();
		}
		if (o.equals(btnTaoTaiKhoan)) {
			String maPhongBan = "";
			if (GUI_Login.nhanVien.getChucVu() == 2) {
				maPhongBan = GUI_Login.nhanVien.getPhongBan().getMaPhongBan();
			} else if (GUI_Login.nhanVien.getChucVu() == 3) {
				maPhongBan = danhSachMaPhongBan.get(cbbPhongBan.getSelectedIndex());
			}
			String tenPhongBan = danhSachPhongBan.get(maPhongBan);
			gui_TaoTk = new GUI_TaoTaiKhoanNhanVien(maPhongBan, tenPhongBan, this);
			gui_TaoTk.setVisible(true);
			if (gui_TaoTk.isVisible()) {
				setEnabled(false);
				flag = 1;
			}

		}
		if (o.equals(btnXoaTaiKhoan)) {
			if (tableDanhSachNhanVien.getSelectedRow() == -1) {
				JOptionPane.showMessageDialog(this, "Vui lòng chọn nhân viên cần xóa");
				return;
			}
			if (GUI_Login.nhanVien.getChucVu() < nhanViens.get(tableDanhSachNhanVien.getSelectedRow()).getChucVu()) {
				JOptionPane.showMessageDialog(this, "Bạn không có quyền xóa người này");
				return;
			}

			int luaChon = JOptionPane.showConfirmDialog(this, "Bạn có chắc chắn xóa nhân viên này?", "Xác nhận",
					JOptionPane.YES_NO_OPTION);
			if (luaChon == JOptionPane.YES_OPTION) {
				try {
					if (nhanVienServices.xoaNhanVien(nhanViens.get(tableDanhSachNhanVien.getSelectedRow()).getMaNV())) {
						JOptionPane.showMessageDialog(this, "Xóa thành công!");
						nhanViens.remove(tableDanhSachNhanVien.getSelectedRow());
						napDanhSachNhanVien();
					}
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		if (o.equals(btnQuanLyThongTinCaNhan)) {
			gui_qlThongTin = new GUI_ThongTinCaNhan();
			gui_qlThongTin.setVisible(true);
			if (gui_qlThongTin.isVisible()) {
				setEnabled(false);
				flag = 2;
			}
		}
		if (o.equals(btnXemCuThe)) {
			if (tableDanhSachNhanVien.getSelectedRow() == -1) {
				JOptionPane.showMessageDialog(this, "Vui lòng chọn nhân viên cần xem");
				return;
			}
			gui_xemChamCong = new GUI_GiaoDienChamCong(nhanViens.get(tableDanhSachNhanVien.getSelectedRow()));
			gui_xemChamCong.setVisible(true);
			if (gui_xemChamCong.isVisible()) {
				setEnabled(false);
				flag = 3;
			}

		}
		if (flag == 1)
			gui_TaoTk.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					setEnabled(true);
				}
				public void windowClosed(WindowEvent e) {
					setEnabled(true);
				}
			});
		else if (flag == 2)
			gui_qlThongTin.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					setEnabled(true);
				}
				public void windowClosed(WindowEvent e) {
					setEnabled(true);
				}
			});
		else if (flag == 3)
			gui_xemChamCong.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					setEnabled(true);
				}
				public void windowClosed(WindowEvent e) {
					setEnabled(true);
				}
			});
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Object o = e.getSource();
		if (o.equals(tableDanhSachNhanVien)) {
			hienThiThongTinLenTextField(tableDanhSachNhanVien.getSelectedRow());
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
