package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.imageio.ImageIO;
import javax.persistence.Embedded;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import entities.ChiTietChamCong;
import entities.DiaChi;
import entities.NhanVien;
import entities.PhongBan;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import services.ChiTietChamCongServices;
import services.NhanVienServices;

public class GUI_GiaoDienChamCong extends JFrame implements ActionListener {

	private JLabel lblMonth, lblYear;
	private JButton btnPrev, btnNext, btnChamCong, btnSuaThongTin, btnDoiMatKhau, btnDangXuat;
	private JTable tblCalendar;
	private JComboBox cmbYear;
	private DefaultTableModel mtblCalendar;
	private JScrollPane stblCalendar;
	private JPanel pnlCalendar;
	private int realDay, realMonth, realYear, currentMonth, currentYear;
	private JTextField txtSoNha, txtTenDuong, txtTenPhuongXa, txtTenQuanHuyen, txtTenTinh, txtEmail, txtHoTen, txtMaNV,
			txtSDT, txtLuongTheoThang, txtSoNgayDiLamCuaNhanVien, txtSoNgayTangCa, txtThanhTien;
	private JLabel labelSoTienDiLamNgayThuong, labelSoTienDiLamNgayLe;
	private UtilDateModel model;
	private JDatePanelImpl datePanel;
	private JDatePickerImpl datePicker;
	private NhanVienServices nvService;
	private SecurityManager securityManager;
	private ChiTietChamCongServices chiTietChamCongServices;
	private List<ChiTietChamCong> chiTietChamCongs;
	private NhanVien nhanVien;
	private DecimalFormat decimalFormat = new DecimalFormat("#,### VNĐ");
	ExecutorService executor = Executors.newFixedThreadPool(5);
	private boolean isExist = true;
	private GUI_GiaoDienChamCong gui_GiaoDienChamCong;
	private GUI_DoiMatKhau gui_doiMK;
	private int flag = -1;

	public GUI_GiaoDienChamCong(NhanVien nhanVien) {
		this.nhanVien = nhanVien;
		gui_GiaoDienChamCong = this;
		setTitle("Chấm công");
		if (nhanVien == null) {
			setSize(1200, 700);
		} else {
			setSize(1200, 400);
		}
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(null);
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(".\\icon_libs\\logo.png"));
		try {
			SecurityManager securityManager = System.getSecurityManager();
			if (securityManager == null) {
				System.setProperty("java.security.policy", "policy\\policyFile.policy");
				securityManager = new SecurityManager();
			}
			nvService = (NhanVienServices) Naming.lookup("rmi://" + GUI_Login.ipAddress + ":1999/nv");
			chiTietChamCongServices = (ChiTietChamCongServices) Naming
					.lookup("rmi://" + GUI_Login.ipAddress + ":1999/ctcc");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		addOtherUIs();
		addUICalendar();
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					Callable<Boolean> threadCheckAccount = new ThreadCheckAccount();
					Future<Boolean> future = executor.submit(threadCheckAccount);
					try {
						isExist = future.get();
						if (isExist == false) {
							int choice = JOptionPane.showConfirmDialog(gui_GiaoDienChamCong, "Tài khoản này đã bị xóa",
									"Thông báo", JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE);
							if (choice == JOptionPane.OK_OPTION) {
								gui_GiaoDienChamCong.dispose();
								new GUI_Login().setVisible(true);
								break;
							}
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		thread.start();
	}

	public void addUICalendar() {
		// Create controls
		lblMonth = new JLabel("January");
		lblYear = new JLabel("Change year:");
		cmbYear = new JComboBox();
		btnPrev = new JButton("<<");
		btnNext = new JButton(">>");
		mtblCalendar = new DefaultTableModel() {
			public boolean isCellEditable(int rowIndex, int mColIndex) {
				return false;
			}
		};
		tblCalendar = new JTable(mtblCalendar);
		stblCalendar = new JScrollPane(tblCalendar);
		pnlCalendar = new JPanel(null);

		// Set border
		pnlCalendar.setBorder(BorderFactory.createTitledBorder("Calendar"));

		// Register action listeners
		btnPrev.addActionListener(this);
		btnNext.addActionListener(this);
		cmbYear.addActionListener(this);

		// Add controls to pane
		add(pnlCalendar);
		pnlCalendar.add(lblMonth);
		pnlCalendar.add(lblYear);
		pnlCalendar.add(cmbYear);
		pnlCalendar.add(btnPrev);
		pnlCalendar.add(btnNext);
		pnlCalendar.add(stblCalendar);

		// Set bounds
		pnlCalendar.setBounds(0, 0, 320, 335);
		lblMonth.setBounds(160 - lblMonth.getPreferredSize().width / 2, 25, 100, 25);
		lblYear.setBounds(10, 305, 80, 20);
		cmbYear.setBounds(230, 305, 80, 20);
		btnPrev.setBounds(10, 25, 50, 25);
		btnNext.setBounds(260, 25, 50, 25);
		stblCalendar.setBounds(10, 50, 300, 250);

		// Get real month/year
		GregorianCalendar cal = new GregorianCalendar(); // Create calendar
		realDay = cal.get(GregorianCalendar.DAY_OF_MONTH); // Get day
		realMonth = cal.get(GregorianCalendar.MONTH); // Get month
		realYear = cal.get(GregorianCalendar.YEAR); // Get year
		currentMonth = realMonth; // Match month and year
		currentYear = realYear;

		// Add headers
		String[] headers = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" }; // All headers
		for (int i = 0; i < 7; i++) {
			mtblCalendar.addColumn(headers[i]);
		}

		tblCalendar.getParent().setBackground(tblCalendar.getBackground()); // Set background

		// No resize/reorder
		tblCalendar.getTableHeader().setResizingAllowed(false);
		tblCalendar.getTableHeader().setReorderingAllowed(false);

		// Single cell selection
		tblCalendar.setColumnSelectionAllowed(true);
		tblCalendar.setRowSelectionAllowed(true);
		tblCalendar.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		// Set row/column count
		tblCalendar.setRowHeight(38);
		mtblCalendar.setColumnCount(7);
		mtblCalendar.setRowCount(6);

		// Populate table
		for (int i = realYear - 100; i <= realYear + 100; i++) {
			cmbYear.addItem(String.valueOf(i));
		}

		// Refresh calendar
		refreshCalendar(realMonth, realYear); // Refresh calendar
	}

	public void addOtherUIs() {

		if (nhanVien == null) {
			btnChamCong = new JButton("Chấm công");
			btnChamCong.setBounds(110, 360, 100, 25);
			add(btnChamCong);

			btnDangXuat = new JButton("Đăng xuất");
			btnDangXuat.setBounds(110, 410, 100, 25);
			add(btnDangXuat);
		}

		JPanel panelInfoUser = new JPanel();
		if (nhanVien == null)
			panelInfoUser.setBounds(320, 0, 857, 650);
		else {
			panelInfoUser.setBounds(320, 0, 857, 335);
		}
		add(panelInfoUser);

		if (nhanVien == null) {
			panelInfoUser.setBorder(BorderFactory.createTitledBorder("Thông tin cá nhân"));
//		panelInfoUser.setLayout(new BoxLayout(panelInfoUser, BoxLayout.Y_AXIS));
			panelInfoUser.setLayout(new FlowLayout());

			JPanel panelTop = new JPanel();
			panelTop.setLayout(new GridLayout(10, 2, 5, 5));
			panelInfoUser.add(panelTop);

			txtSoNha = new JTextField(30);
			txtTenDuong = new JTextField(30);
			txtTenPhuongXa = new JTextField(30);
			txtTenQuanHuyen = new JTextField(30);
			txtTenTinh = new JTextField(30);
			txtEmail = new JTextField(30);
			txtHoTen = new JTextField(30);
			txtMaNV = new JTextField(30);
			txtMaNV.setEnabled(false);
			txtMaNV.setDisabledTextColor(Color.lightGray);
			txtMaNV.setBackground(Color.DARK_GRAY);

			txtSDT = new JTextField(30);

			JLabel labelSoNha = new JLabel("Số nhà:");
			JLabel labelTenDuong = new JLabel("Tên đường:");
			JLabel labelTenPhuong = new JLabel("Tên phường xã:");
			JLabel labelTenQuan = new JLabel("Tên quận huyện:");
			JLabel labelTenTinh = new JLabel("Tên tỉnh:");
			JLabel labelEmail = new JLabel("Email:");
			JLabel labelHoTen = new JLabel("Họ tên:");
			JLabel labelMaNV = new JLabel("Mã NV:");
			JLabel labelSDT = new JLabel("Số điện thoại:");

			List<JTextField> textFields = Arrays.asList(new JTextField[] { txtMaNV, txtHoTen, txtEmail, txtSDT,
					txtSoNha, txtTenDuong, txtTenPhuongXa, txtTenQuanHuyen, txtTenTinh });
			List<JLabel> labels = Arrays.asList(new JLabel[] { labelMaNV, labelHoTen, labelEmail, labelSDT, labelSoNha,
					labelTenDuong, labelTenPhuong, labelTenQuan, labelTenTinh });

			for (int i = 0; i < textFields.size(); i++) {
				panelTop.add(labels.get(i));
				panelTop.add(textFields.get(i));
			}

			JLabel labelNgaySinh = new JLabel("Ngày sinh:");
			model = new UtilDateModel();
			datePanel = new JDatePanelImpl(model);
			datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());

			panelTop.add(labelNgaySinh);
			panelTop.add(datePicker);

			JPanel panelBottom = new JPanel();
//		panelBottom.setLayout(new BoxLayout(panelBottom, BoxLayout.X_AXIS));
			panelBottom.setLayout(new FlowLayout(FlowLayout.CENTER));

			btnSuaThongTin = new JButton("Sửa thông tin");

			try {
				BufferedImage bufferedImage = ImageIO.read(GUI_Login.class.getResource("/icon_libs/edit-icon.png"));
				btnSuaThongTin = new JButton("Sửa thông tin", new ImageIcon(bufferedImage));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

//		btnLuuThongTin = new JButton("Lưu thông tin", new ImageIcon("icon_libs/save-icon.png"));
			btnDoiMatKhau = new JButton("Đổi mật khẩu");
			try {
				BufferedImage bufferedImage = ImageIO
						.read(GUI_Login.class.getResource("/icon_libs/modify-password-icon.png"));
				btnDoiMatKhau = new JButton("Đổi mật khẩu", new ImageIcon(bufferedImage));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//		btnDoiMatKhau.setPreferredSize(btnSuaThongTin.getPreferredSize());

			panelInfoUser.add(panelBottom);
			panelBottom.add(btnSuaThongTin);
//		panelBottom.add(btnLuuThongTin);
			panelBottom.add(btnDoiMatKhau);
		} else {
			panelInfoUser.setBorder(BorderFactory.createTitledBorder("Bảng lương"));
//			panelInfoUser.setLayout(new BoxLayout(panelInfoUser, BoxLayout.Y_AXIS));
			panelInfoUser.setLayout(new BoxLayout(panelInfoUser, BoxLayout.Y_AXIS));

			txtLuongTheoThang = new JTextField(20);
			txtSoNgayDiLamCuaNhanVien = new JTextField(20);
			txtSoNgayTangCa = new JTextField(20);
			txtThanhTien = new JTextField(20);
			labelSoTienDiLamNgayThuong = new JLabel(decimalFormat.format(0));
			labelSoTienDiLamNgayLe = new JLabel(decimalFormat.format(0));

			JLabel labelLuongTheoThang = new JLabel("Lương theo tháng: ");
			JLabel labelSoNgayDiLamCuaNhanVien = new JLabel("Số ngày đi làm của nhân viên: ");
			JLabel labelSoNgayTangCa = new JLabel("Số ngày tăng ca: ");
			JLabel labelThanhTien = new JLabel("Lương thực tế: ");
			JLabel labelTitleSoTienDiLamNgayThuong = new JLabel("Số tiền đi làm ngày bình thường: ");
			JLabel labelTitleSoTienDilamTangCa = new JLabel("Số tiền đi làm tăng ca: ");

			labelLuongTheoThang.setPreferredSize(labelSoNgayDiLamCuaNhanVien.getPreferredSize());
			labelSoNgayTangCa.setPreferredSize(labelSoNgayDiLamCuaNhanVien.getPreferredSize());
			labelThanhTien.setPreferredSize(labelSoNgayDiLamCuaNhanVien.getPreferredSize());
			labelTitleSoTienDilamTangCa.setPreferredSize(labelTitleSoTienDiLamNgayThuong.getPreferredSize());

			JPanel panelLuongTheoThang = new JPanel(new FlowLayout(FlowLayout.LEFT));
			panelLuongTheoThang.add(labelLuongTheoThang);
			panelLuongTheoThang.add(txtLuongTheoThang);
			panelInfoUser.add(panelLuongTheoThang);

			JPanel panelSoNgayDiLam = new JPanel(new FlowLayout(FlowLayout.LEFT));
			panelSoNgayDiLam.add(labelSoNgayDiLamCuaNhanVien);
			panelSoNgayDiLam.add(txtSoNgayDiLamCuaNhanVien);
			panelSoNgayDiLam.add(labelTitleSoTienDilamTangCa);
			panelSoNgayDiLam.add(labelSoTienDiLamNgayLe);
			panelInfoUser.add(panelSoNgayDiLam);

			JPanel panelSoNgayTangCa = new JPanel(new FlowLayout(FlowLayout.LEFT));
			panelSoNgayTangCa.add(labelSoNgayTangCa);
			panelSoNgayTangCa.add(txtSoNgayTangCa);
			panelSoNgayTangCa.add(labelTitleSoTienDiLamNgayThuong);
			panelSoNgayTangCa.add(labelSoTienDiLamNgayThuong);
			panelInfoUser.add(panelSoNgayTangCa);

			JPanel panelThanhTien = new JPanel(new FlowLayout(FlowLayout.LEFT));
			panelThanhTien.add(labelThanhTien);
			panelThanhTien.add(txtThanhTien);
			panelInfoUser.add(panelThanhTien);

		}

		if (nhanVien == null) {
			btnChamCong.addActionListener(this);
			btnSuaThongTin.addActionListener(this);
			btnDangXuat.addActionListener(this);
			btnDoiMatKhau.addActionListener(this);
		}

		if (nhanVien == null) {
			napThongTinNhanVien();
		}

		try {
			napDanhSachNgayChamCong();
			if (kiemTraChamCong()) {
				btnChamCong.setEnabled(false);
			}

			if (nhanVien != null) {

				try {
					napThongTinChamCong(LocalDate.now().getMonthValue(), LocalDate.now().getYear());
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void refreshCalendar(int month, int year) {
		String[] months = { "Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8",
				"Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12" };
		int nod, som; // Number Of Days, Start Of Month

		// Prepare buttons
		btnPrev.setEnabled(true); // Enable buttons at first
		btnNext.setEnabled(true);
		if (month == 0 && year <= realYear - 10) {
			btnPrev.setEnabled(false);
		} // Too early
		if (month == 11 && year >= realYear + 100) {
			btnNext.setEnabled(false);
		} // Too late
		lblMonth.setText(months[month]); // Refresh the month label (at the top)
		lblMonth.setBounds(160 - lblMonth.getPreferredSize().width / 2, 25, 180, 25); // Re-align label with calendar
		cmbYear.setSelectedItem(String.valueOf(year)); // Select the correct year in the combo box

		// Get number of days and start of month
		GregorianCalendar cal = new GregorianCalendar(year, month, 1);
		nod = cal.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
		som = cal.get(GregorianCalendar.DAY_OF_WEEK);

		// Clear table
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				mtblCalendar.setValueAt(null, i, j);
			}
		}

		for (int i = 1; i <= nod; i++) {
			int row = (Integer) ((i + som - 2) / 7);
			int column = (i + som - 2) % 7;
			mtblCalendar.setValueAt(i, row, column);
		}

		tblCalendar.setDefaultRenderer(tblCalendar.getColumnClass(0), new tblCalendarRenderer());

	}

	public void napThongTinChamCong(int thang, int nam) throws RemoteException {
		int soNgayDiLamTheoQuyDinh = getNumDateToWorkInMonth(thang, nam);
		txtLuongTheoThang.setText(decimalFormat.format(nhanVien.getLuongTheoThang()));
		txtSoNgayDiLamCuaNhanVien.setText(Integer.toString(chiTietChamCongs.size()) + "/" + soNgayDiLamTheoQuyDinh);
		int soNgayTangCa = chiTietChamCongServices.getNgayNghiNhungLam(nhanVien.getMaNV(), thang, nam);
		txtSoNgayTangCa.setText(Integer.toString(soNgayTangCa));
		int soTienDiLamNgayBinhThuong = nhanVien.getLuongTheoThang() / soNgayDiLamTheoQuyDinh * chiTietChamCongs.size();
		int soTienDiLamTangCa = nhanVien.getLuongTheoThang() / soNgayDiLamTheoQuyDinh * soNgayTangCa;
		txtThanhTien.setText(decimalFormat.format(soTienDiLamNgayBinhThuong + soTienDiLamTangCa));
	}

	public int getNumDateToWorkInMonth(int thang, int nam) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(nam, thang - 1, 1);
		int maximumDate = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		int result = 0;
		for (int i = 1; i <= maximumDate; i++) {
			if (Judger.judgeLoaiNgay(LocalDate.of(nam, thang, i)) == 1) {
				result++;
			}
		}
		return result;
	}

	public void napThongTinNhanVien() {
		if (nhanVien != null) {
			txtMaNV.setText(nhanVien.getMaNV());
			txtHoTen.setText(nhanVien.getHoTen());
			txtEmail.setText(nhanVien.getEmail());
			txtSDT.setText(nhanVien.getSdt());
			txtSoNha.setText(nhanVien.getDiaChi().getSoNha());
			txtTenDuong.setText(nhanVien.getDiaChi().getTenDuong());
			txtTenPhuongXa.setText(nhanVien.getDiaChi().getTenPhuongXa());
			txtTenQuanHuyen.setText(nhanVien.getDiaChi().getTenQuanHuyen());
			txtTenTinh.setText(nhanVien.getDiaChi().getTenTinhTP());
			Calendar calendar = Calendar.getInstance();
			calendar.set(nhanVien.getNgaySinh().getYear(), nhanVien.getNgaySinh().getMonthValue() - 1,
					nhanVien.getNgaySinh().getDayOfMonth());
			model.setValue(calendar.getTime());
			return;
		}
		txtMaNV.setText(GUI_Login.nhanVien.getMaNV());
		txtHoTen.setText(GUI_Login.nhanVien.getHoTen());
		txtEmail.setText(GUI_Login.nhanVien.getEmail());
		txtSDT.setText(GUI_Login.nhanVien.getSdt());
		txtSoNha.setText(GUI_Login.nhanVien.getDiaChi().getSoNha());
		txtTenDuong.setText(GUI_Login.nhanVien.getDiaChi().getTenDuong());
		txtTenPhuongXa.setText(GUI_Login.nhanVien.getDiaChi().getTenPhuongXa());
		txtTenQuanHuyen.setText(GUI_Login.nhanVien.getDiaChi().getTenQuanHuyen());
		txtTenTinh.setText(GUI_Login.nhanVien.getDiaChi().getTenTinhTP());
		Calendar calendar = Calendar.getInstance();
		calendar.set(GUI_Login.nhanVien.getNgaySinh().getYear(), GUI_Login.nhanVien.getNgaySinh().getMonthValue() - 1,
				GUI_Login.nhanVien.getNgaySinh().getDayOfMonth());
		model.setValue(calendar.getTime());
	}

	public void napDanhSachNgayChamCong() throws RemoteException {
		LocalDate thisDate = LocalDate.now();
		if (nhanVien == null) {
			chiTietChamCongs = chiTietChamCongServices.getDanhSachChiTietChamCong(GUI_Login.nhanVien.getMaNV(),
					thisDate.getMonthValue(), thisDate.getYear());
		} else {
			chiTietChamCongs = chiTietChamCongServices.getDanhSachChiTietChamCong(nhanVien.getMaNV(),
					thisDate.getMonthValue(), thisDate.getYear());
		}
		for (ChiTietChamCong ngay : chiTietChamCongs) {
			System.out.println(ngay.getNgayChamCong().getNgay());
		}
	}

	public boolean kiemTraChamCong() {
		LocalDate thisDate = LocalDate.now();
		boolean isChamCong = false;
		for (ChiTietChamCong chiTietChamCong : chiTietChamCongs) {
			if (chiTietChamCong.getNgayChamCong().getNgay().equals(thisDate)) {
				isChamCong = true;
				break;
			}
		}
		return isChamCong;
	}

	public boolean isValidateData() {
		String hoTen = txtHoTen.getText().trim();
		String email = txtEmail.getText().trim();
		String soDienThoai = txtSDT.getText().trim();
		String soNha = txtSoNha.getText().trim();
		String tenDuong = txtTenDuong.getText().trim();
		String tenPhuong = txtTenPhuongXa.getText().trim();
		String tenQuan = txtTenQuanHuyen.getText().trim();
		String tenTinh = txtTenTinh.getText().trim();
		LocalDate ngaySinh = LocalDate.of(model.getYear(), model.getMonth() + 1, model.getDay());

		if (hoTen.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên nhân viên không được rỗng");
			txtHoTen.requestFocus();
			return false;
		} else if (!hoTen.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên nhân viên không được chứa kí tự đặc biệt");
			txtHoTen.requestFocus();
			return false;
		}
		if (email.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Email nhân viên không được rỗng");
			txtEmail.requestFocus();
			return false;
		} else if (!email.matches("^[a-z][a-z0-9_\\.]{1,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$")) {
			JOptionPane.showMessageDialog(this, "Error: Email nhân viên không đúng định dạng");
			txtEmail.requestFocus();
			return false;
		}
		if (soDienThoai.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Số điện thoại nhân viên không được rỗng");
			txtSDT.requestFocus();
			return false;
		} else if (!soDienThoai.matches("^[0-9]{10}$")) {
			JOptionPane.showMessageDialog(this, "Error: Số điện thoại nhân viên chỉ bao gồm 10 chữ số");
			txtSDT.requestFocus();
			return false;
		}
		if (soNha.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Số nhà không được rỗng");
			txtSoNha.requestFocus();
			return false;
		} else if (!soNha.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Số nhà không được chứa kí tự đặc biệt");
			txtSoNha.requestFocus();
			return false;
		}
		if (tenDuong.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên đường không được rỗng");
			txtTenDuong.requestFocus();
			return false;
		} else if (!tenDuong.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên đường không được chứa kí tự đặc biệt");
			txtTenDuong.requestFocus();
			return false;
		}
		if (tenPhuong.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên phường không được rỗng");
			txtTenPhuongXa.requestFocus();
			return false;
		} else if (!tenPhuong.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên phường không được chứa kí tự đặc biệt");
			txtTenPhuongXa.requestFocus();
			return false;
		}
		if (tenQuan.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên quận không được rỗng");
			txtTenQuanHuyen.requestFocus();
			return false;
		} else if (!tenQuan.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên quận không được chứa kí tự đặc biệt");
			txtTenQuanHuyen.requestFocus();
			return false;
		}
		if (tenTinh.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên tỉnh không được rỗng");
			txtTenTinh.requestFocus();
			return false;
		} else if (!tenTinh.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên tỉnh không được chứa kí tự đặc biệt");
			txtTenTinh.requestFocus();
			return false;
		}

		if (LocalDate.now().getYear() - ngaySinh.getYear() < 18) {
			JOptionPane.showMessageDialog(this, "Ngày sinh không hợp lệ, chưa đủ 18 tuổi");
			return false;
		}
		return true;
	}

	public boolean modifyNhanVienFromTextField() throws RemoteException {
		DiaChi diaChi = new DiaChi(txtSoNha.getText(), txtTenDuong.getText(), txtTenPhuongXa.getText(),
				txtTenQuanHuyen.getText(), txtTenTinh.getText());
		String email = txtEmail.getText();
		String hoTen = txtHoTen.getText();
		LocalDate ngaySinh = LocalDate.of(model.getYear(), model.getMonth() + 1, model.getDay());
		String sdt = txtSDT.getText();
		GUI_Login.nhanVien.setDiaChi(diaChi);
		GUI_Login.nhanVien.setEmail(email);
		GUI_Login.nhanVien.setHoTen(hoTen);
		GUI_Login.nhanVien.setNgaySinh(ngaySinh);
		GUI_Login.nhanVien.setSdt(sdt);
		return nvService.doiThongTin(GUI_Login.nhanVien);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		if (o == btnPrev) {
			if (currentMonth == 0) { // Back one year
				currentMonth = 11;
				currentYear -= 1;
			} else { // Back one month
				currentMonth -= 1;
			}
			try {
				if (nhanVien == null) {
					chiTietChamCongs = chiTietChamCongServices.getDanhSachChiTietChamCong(GUI_Login.nhanVien.getMaNV(),
							currentMonth + 1, currentYear);
				} else {
					chiTietChamCongs = chiTietChamCongServices.getDanhSachChiTietChamCong(nhanVien.getMaNV(),
							currentMonth + 1, currentYear);
					napThongTinChamCong(currentMonth + 1, currentYear);
				}

			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			refreshCalendar(currentMonth, currentYear);
		} else if (o == btnNext) {
			if (currentMonth == 11) { // Foward one year
				currentMonth = 0;
				currentYear += 1;
			} else { // Foward one month
				currentMonth += 1;
			}
			try {
				if (nhanVien == null) {
					chiTietChamCongs = chiTietChamCongServices.getDanhSachChiTietChamCong(GUI_Login.nhanVien.getMaNV(),
							currentMonth + 1, currentYear);
				} else {
					chiTietChamCongs = chiTietChamCongServices.getDanhSachChiTietChamCong(nhanVien.getMaNV(),
							currentMonth + 1, currentYear);
					napThongTinChamCong(currentMonth + 1, currentYear);
				}
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			refreshCalendar(currentMonth, currentYear);
		} else if (o == cmbYear) {
			if (cmbYear.getSelectedItem() != null) {
				String b = cmbYear.getSelectedItem().toString();
				currentYear = Integer.parseInt(b);
				try {
					if (nhanVien == null) {
						chiTietChamCongs = chiTietChamCongServices.getDanhSachChiTietChamCong(
								GUI_Login.nhanVien.getMaNV(), currentMonth + 1, currentYear);
					} else {
						chiTietChamCongs = chiTietChamCongServices.getDanhSachChiTietChamCong(nhanVien.getMaNV(),
								currentMonth + 1, currentYear);
						napThongTinChamCong(currentMonth + 1, currentYear);
					}
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				refreshCalendar(currentMonth, currentYear);
			}
		} else if (o == btnChamCong) {
			LocalDate thisDate = LocalDate.now();
			boolean isChamCong;
			try {
				isChamCong = chiTietChamCongServices.chamCong(GUI_Login.nhanVien.getMaNV(), thisDate);
				if (isChamCong == true) {
					refreshCalendar(thisDate.getMonthValue() - 1, thisDate.getYear());
					JOptionPane.showMessageDialog(this, "Chấm công thành công!");
				} else {
					JOptionPane.showMessageDialog(this,
							"Chấm công thất bại hoặc bạn đã chấm công rồi, vui lòng thử lại sau!");
				}
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (kiemTraChamCong()) {
				btnChamCong.setEnabled(false);
			}
		}
		if (o == btnSuaThongTin) {
//			System.out.println(model.getMonth());
			if (!isValidateData()) {
				return;
			}
			try {
				if (modifyNhanVienFromTextField()) {
					JOptionPane.showMessageDialog(this, "Đổi thông tin thành công!");
				} else {
					JOptionPane.showMessageDialog(this, "Đổi thông tin không thành công!");
				}
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		if (o == btnDangXuat) {
			int choice = JOptionPane.showConfirmDialog(this, "Bạn có muốn đăng xuất?", "Xác nhận",
					JOptionPane.YES_NO_OPTION);
			if (choice == JOptionPane.YES_OPTION) {
				new GUI_Login().setVisible(true);
				dispose();
			}

		}
		if (o == btnDoiMatKhau) {
			gui_doiMK = new GUI_DoiMatKhau();
			if (gui_doiMK.isVisible()) {
				this.setEnabled(false);
				flag = 1;
			}

		}
		if (flag == 1)
			gui_doiMK.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					setEnabled(true);
				}

				public void windowClosed(WindowEvent e) {
					setEnabled(true);
				}
			});
	}

	public static void main(String[] args) {
		new GUI_Login().setVisible(true);
//		Calendar cal = new GregorianCalendar(2019, Calendar.SEPTEMBER, 1);
//		Calendar nextMonth = new GregorianCalendar(2019, Calendar.OCTOBER, 1);
//		Date date = cal.getTime();
//		System.out.println("Ngày hiện tại là: " + (cal.getActualMaximum(Calendar.DAY_OF_MONTH)));
	}

	class tblCalendarRenderer extends DefaultTableCellRenderer {
		public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused,
				int row, int column) {
			super.getTableCellRendererComponent(table, value, selected, focused, row, column);
			boolean valueIsContainInList = false;
			for (ChiTietChamCong ngay : chiTietChamCongs) {
				if (value != null && ngay.isDaChamCong() == true
						&& ngay.getNgayChamCong().getNgay().getDayOfMonth() == (int) value) {
					valueIsContainInList = true;
					break;
				}
			}
			if (valueIsContainInList) {
				setBackground(new Color(34, 139, 34));
			} else if (column == 0 || column == 6) { // Week-end
				setBackground(new Color(255, 220, 220));
			} else { // Week
				setBackground(new Color(255, 255, 255));
			}
			if (value != null) {
				if (Integer.parseInt(value.toString()) == realDay && currentMonth == realMonth
						&& currentYear == realYear) { // Today
					if (valueIsContainInList) {
						setBackground(new Color(34, 139, 34));
					} else {
						setBackground(new Color(220, 220, 255));
					}
				}
			}
			setBorder(null);
			setForeground(Color.black);
			return this;
		}
	}

}
