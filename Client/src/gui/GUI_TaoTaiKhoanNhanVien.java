package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.hibernate.hql.ast.origin.hql.parse.HQLParser.object_key_return;

import dao.MD5;
import entities.DiaChi;
import entities.NhanVien;
import entities.PhongBan;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import services.ChiTietChamCongServices;
import services.NhanVienServices;

public class GUI_TaoTaiKhoanNhanVien extends JFrame implements ActionListener {

	private JTextField txtHoTen, txtEmail, txtSoDienThoai, txtSoNha, txtTenDuong, txtTenPhuongXa, txtTenQuanHuyen,
			txtTenTinh, txtMaSo, txtTienLuong;
	private JPasswordField txtMatKhau, txtXacNhanMatKhau;
	private UtilDateModel dateModel;
	private JDatePickerImpl datePickerImpl;
	private JDatePanelImpl datePanelImpl;
	private JComboBox<String> cbbChucVu;
	private JButton btnSave;
	private final HashMap<String, Integer> danhSachChucVu = new HashMap<String, Integer>();
	private NhanVienServices nhanVienServices;
	private String maPhongBan;
	private String tenPhongBan;
	private GUI_QuanLyNhanVien quanLyNhanVien;
	
	public GUI_TaoTaiKhoanNhanVien(String maPhongBan, String tenPhongBan, GUI_QuanLyNhanVien quanLyNhanVien) {
		this.maPhongBan = maPhongBan;
		this.tenPhongBan = tenPhongBan;
		this.quanLyNhanVien = quanLyNhanVien;
		setTitle("Tạo tài khoản nhân viên");
		setSize(900, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);
		try {
			SecurityManager securityManager = System.getSecurityManager();
			if (securityManager == null) {
				System.setProperty("java.security.policy", "policy\\policyFile.policy");
				securityManager = new SecurityManager();
			}
			nhanVienServices = (NhanVienServices) Naming.lookup("rmi://" + GUI_Login.ipAddress + ":1999/nv");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		addUI();
	}

	public void addUI() {
//		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.X_AXIS));
		this.setLayout(new FlowLayout(FlowLayout.LEFT));

//		JPanel panelForImageInfo = new JPanel();
//		JLabel labelImageInfo = new JLabel(new ImageIcon("icon_libs/info-icon.png"));
//		this.add(panelForImageInfo);
//		panelForImageInfo.setPreferredSize(new Dimension(900, labelImageInfo.getPreferredSize().height));
//		panelForImageInfo.add(labelImageInfo);

		txtMaSo = new JTextField(15);
		txtHoTen = new JTextField(15);
		txtEmail = new JTextField(15);
		txtSoDienThoai = new JTextField(15);
		txtSoNha = new JTextField(15);
		txtTenDuong = new JTextField(15);
		txtTenPhuongXa = new JTextField(15);
		txtTenQuanHuyen = new JTextField(15);
		txtTenTinh = new JTextField(15);
		txtMatKhau = new JPasswordField(15);
		txtXacNhanMatKhau = new JPasswordField(15);
		txtTienLuong = new JTextField(15);

		JLabel labelTieuDeMaSo = new JLabel("Mã số: ");

		List<JTextField> textFields = Arrays
				.asList(new JTextField[] { txtMaSo, txtHoTen, txtEmail, txtSoDienThoai, txtSoNha, txtTenDuong,
						txtTenPhuongXa, txtTenQuanHuyen, txtTenTinh, txtMatKhau, txtXacNhanMatKhau, txtTienLuong });
		List<String> titles = Arrays.asList(new String[] { "Mã nhân viên: ", "Họ tên: ", "Email: ", "Số điện thoại: ",
				"Số nhà: ", "Tên đường: ", "Tên phường: ", "Tên quận: ", "Tên tỉnh: ", "Mật khẩu: ",
				"Xác nhận mật khẩu: ", "Tiền lương: " });

		for (int i = 0; i < textFields.size(); i++) {
			this.add(createPanel(titles.get(i), textFields.get(i)));
		}

		dateModel = new UtilDateModel();
		datePanelImpl = new JDatePanelImpl(dateModel);
		datePickerImpl = new JDatePickerImpl(datePanelImpl, new DateLabelFormatter());
		datePickerImpl.setPreferredSize(new Dimension(txtHoTen.getPreferredSize().width, 30));
		dateModel.setSelected(true);

		JLabel labelNgaySinh = new JLabel("Ngày sinh: ");
		labelNgaySinh.setPreferredSize(new JLabel("Xác nhận mật khẩu: ").getPreferredSize());

		JPanel panelNgaySinh = new JPanel(new FlowLayout());
		panelNgaySinh.add(labelNgaySinh);
		panelNgaySinh.add(datePickerImpl);

		this.add(panelNgaySinh);

		cbbChucVu = new JComboBox<String>();
		ArrayList<String> listChucVu = new ArrayList<String>(Arrays.asList(new String[] { "Nhân viên", "Trưởng phòng", "Quản lí" }));
		if (GUI_Login.nhanVien.getChucVu() == 2) {
			listChucVu.remove(listChucVu.size() - 1);
		}
		for (int i = 1; i <= listChucVu.size(); i++) {
			danhSachChucVu.put(listChucVu.get(i - 1), i);
			cbbChucVu.addItem(listChucVu.get(i - 1));
		}
		JPanel panelChucVu = new JPanel(new FlowLayout());
		JLabel labelChucVu = new JLabel("Chức vụ: ");
		labelChucVu.setPreferredSize(new JLabel("Xác nhận mật khẩu: ").getPreferredSize());
		cbbChucVu.setPreferredSize(txtEmail.getPreferredSize());
		panelChucVu.add(labelChucVu);
		panelChucVu.add(cbbChucVu);
		this.add(panelChucVu);

		JPanel panelForButton = new JPanel(new FlowLayout());
		
		btnSave = new JButton("Hoàn tất");
		try {
			BufferedImage bufferedImage = ImageIO.read(GUI_Login.class.getResource("/icon_libs/create-icon.png"));
			btnSave = new JButton("Hoàn tất", new ImageIcon(bufferedImage));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		panelForButton.add(btnSave);

		panelForButton.setPreferredSize(new Dimension(900, 50));
		
		dateModel.setYear(LocalDate.now().getYear() - 18);
		dateModel.setSelected(true);

		btnSave.addActionListener(this);

		this.add(panelForButton);
	}

	public JPanel createPanel(String titleOfLabel, JTextField textField) {
		JLabel label = new JLabel(titleOfLabel);
		JLabel labelModel = new JLabel("Xác nhận mật khẩu: ");
		label.setPreferredSize(labelModel.getPreferredSize());
		JPanel panelFlow = new JPanel(new FlowLayout(FlowLayout.CENTER));
		panelFlow.add(label);
		panelFlow.add(textField);
		return panelFlow;
	}

	public boolean isValidateData() {
		try {
			int tienLuong = Integer.valueOf(txtTienLuong.getText());
		} catch (NumberFormatException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Bạn nhập không đúng định dạng số trong tiền lương");
			txtTienLuong.requestFocus();
			return false;
		}
		String maNhanVien = txtMaSo.getText().trim();
		String hoTen = txtHoTen.getText().trim();
		String email = txtEmail.getText().trim();
		String soDienThoai = txtSoDienThoai.getText().trim();
		String soNha = txtSoNha.getText().trim();
		String tenDuong = txtTenDuong.getText().trim();
		String tenPhuong = txtTenPhuongXa.getText().trim();
		String tenQuan = txtTenQuanHuyen.getText().trim();
		String tenTinh = txtTenTinh.getText().trim();
		String matKhau = String.valueOf(txtMatKhau.getPassword());
		LocalDate ngaySinh = LocalDate.of(dateModel.getYear(), dateModel.getMonth() + 1, dateModel.getDay());

		if (maNhanVien.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Mã nhân viên không được rỗng");
			txtMaSo.requestFocus();
			return false;
		} else if (!maNhanVien.matches("^[0-9a-zA-Z]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Mã nhân viên không được chứa kí tự đặc biệt");
			txtMaSo.requestFocus();
			return false;
		}
		if (hoTen.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên nhân viên không được rỗng");
			txtHoTen.requestFocus();
			return false;
		} else if (!hoTen.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên nhân viên không được chứa kí tự đặc biệt");
			txtHoTen.requestFocus();
			return false;
		}
		if (email.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Email nhân viên không được rỗng");
			txtEmail.requestFocus();
			return false;
		} else if (!email.matches("^[a-z][a-z0-9_\\.]{1,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$")) {
			JOptionPane.showMessageDialog(this, "Error: Email nhân viên không đúng định dạng");
			txtEmail.requestFocus();
			return false;
		}
		if (soDienThoai.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Số điện thoại nhân viên không được rỗng");
			txtSoDienThoai.requestFocus();
			return false;
		}
		else if (!soDienThoai.matches("^[0-9]{10}$")) {
			JOptionPane.showMessageDialog(this, "Error: Số điện thoại nhân viên chỉ bao gồm 10 chữ số");
			txtSoDienThoai.requestFocus();
			return false;
		}
		if (soNha.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Số nhà không được rỗng");
			txtSoNha.requestFocus();
			return false;
		} else if (!soNha.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Số nhà không được chứa kí tự đặc biệt");
			txtSoNha.requestFocus();
			return false;
		}
		if (tenDuong.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên đường không được rỗng");
			txtTenDuong.requestFocus();
			return false;
		} else if (!tenDuong.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên đường không được chứa kí tự đặc biệt");
			txtTenDuong.requestFocus();
			return false;
		}
		if (tenPhuong.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên phường không được rỗng");
			txtTenPhuongXa.requestFocus();
			return false;
		} else if (!tenPhuong.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên phường không được chứa kí tự đặc biệt");
			txtTenPhuongXa.requestFocus();
			return false;
		}
		if (tenQuan.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên quận không được rỗng");
			txtTenQuanHuyen.requestFocus();
			return false;
		} else if (!tenQuan.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên quận không được chứa kí tự đặc biệt");
			txtTenQuanHuyen.requestFocus();
			return false;
		}
		if (tenTinh.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên tỉnh không được rỗng");
			txtTenTinh.requestFocus();
			return false;
		} else if (!tenTinh.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên tỉnh không được chứa kí tự đặc biệt");
			txtTenTinh.requestFocus();
			return false;
		}
		if (matKhau.length() < 8) {
			JOptionPane.showMessageDialog(this, "Mật khẩu phải dài ít nhất 8 kí tự");
			txtMatKhau.requestFocus();
			return false;
		}
		else if (!matKhau.equals(String.valueOf(txtXacNhanMatKhau.getPassword()))) {
			JOptionPane.showMessageDialog(this, "Xác nhận mật khẩu không đúng, vui lòng nhập lại");
			txtXacNhanMatKhau.requestFocus();
			return false;
		}
		
		if (LocalDate.now().getYear() - ngaySinh.getYear() < 18) {
			JOptionPane.showMessageDialog(this, "Ngày sinh không hợp lệ, chưa đủ 18 tuổi");
			return false;
		}
		return true;
	}

	public NhanVien createNhanVien() {
		String maNhanVien = txtMaSo.getText().trim();
		String hoTen = txtHoTen.getText().trim();
		String email = txtEmail.getText().trim();
		String soDienThoai = txtSoDienThoai.getText().trim();
		String soNha = txtSoNha.getText().trim();
		String tenDuong = txtTenDuong.getText().trim();
		String tenPhuong = txtTenPhuongXa.getText().trim();
		String tenQuan = txtTenQuanHuyen.getText().trim();
		String tenTinh = txtTenTinh.getText().trim();
		String matKhau = String.valueOf(txtMatKhau.getPassword());
		int tienLuong = Integer.valueOf(txtTienLuong.getText().trim());
		LocalDate ngaySinh = LocalDate.of(dateModel.getYear(), dateModel.getMonth() + 1, dateModel.getDay());
		PhongBan phongBan = new PhongBan(maPhongBan, tenPhongBan);
		int chucVu = danhSachChucVu.get(cbbChucVu.getSelectedItem());
		NhanVien nhanVien = new NhanVien();
		nhanVien.setMaNV(maNhanVien);
		nhanVien.setHoTen(hoTen);
		nhanVien.setEmail(email);
		nhanVien.setSdt(soDienThoai);
		DiaChi diaChi = new DiaChi(soNha, tenDuong, tenPhuong, tenQuan, tenTinh);
		nhanVien.setDiaChi(diaChi);
		nhanVien.setMatKhau(matKhau);
		nhanVien.setNgaySinh(ngaySinh);
		nhanVien.setChucVu(chucVu);
		nhanVien.setLuongTheoThang(tienLuong);

		if (chucVu != 3) {
			nhanVien.setPhongBan(phongBan);
		}

		return nhanVien;
	}

	public static void main(String[] args) {
		new GUI_Login().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		if (o.equals(btnSave)) {
			if (!isValidateData()) {
				return;
			}
			NhanVien nhanVien = createNhanVien();
			try {
				if (nhanVienServices.createNhanVien(nhanVien)) {
					quanLyNhanVien.getNhanViens().add(nhanVien);
					quanLyNhanVien.napDanhSachNhanVien();
					JOptionPane.showMessageDialog(quanLyNhanVien, "Tạo nhân viên thành công");
					dispose();
				} else {
					JOptionPane.showMessageDialog(this,
							"Tạo tài khoản không thành công! Vui lòng kiểm tra lại mã nhân viên");
				}
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

}
