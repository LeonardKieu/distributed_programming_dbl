package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import entities.DiaChi;
import entities.NhanVien;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import services.ChiTietChamCongServices;
import services.NhanVienServices;

public class GUI_ThongTinCaNhan extends JFrame implements ActionListener {

	private JTextField txtHoTen, txtEmail, txtSoDienThoai, txtSoNha, txtTenDuong, txtTenPhuongXa, txtTenQuanHuyen,
			txtTenTinh;
	private UtilDateModel dateModel;
	private JDatePickerImpl datePickerImpl;
	private JDatePanelImpl datePanelImpl;
	private JLabel labelMaSo;
	private JButton btnDoiMatKhau, btnChinhSua;
	private NhanVienServices nvService;
	private SecurityManager securityManager;
	private GUI_DoiMatKhau gui_doiMK;

	public GUI_ThongTinCaNhan() {
		setTitle("Thông tin cá nhân");
		setSize(900, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);
		try {
			SecurityManager securityManager = System.getSecurityManager();
			if (securityManager == null) {
				System.setProperty("java.security.policy", "policy\\policyFile.policy");
				securityManager = new SecurityManager();
			}
			nvService = (NhanVienServices) Naming.lookup("rmi://" + GUI_Login.ipAddress + ":1999/nv");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		addUI();
	}

	public void addUI() {
//		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.X_AXIS));
		this.setLayout(new FlowLayout());

		JPanel panelForImageInfo = new JPanel();
		JLabel labelImageInfo = new JLabel(new ImageIcon("/icon_libs/info-icon.png"));
		try {
			BufferedImage bufferedImage = ImageIO.read(GUI_Login.class.getResource("/icon_libs/info-icon.png"));
			labelImageInfo = new JLabel(new ImageIcon(bufferedImage));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.add(panelForImageInfo);
		panelForImageInfo.setPreferredSize(new Dimension(900, labelImageInfo.getPreferredSize().height));
		panelForImageInfo.add(labelImageInfo);

		txtHoTen = new JTextField(15);
		txtEmail = new JTextField(15);
		txtSoDienThoai = new JTextField(15);
		txtSoNha = new JTextField(15);
		txtTenDuong = new JTextField(15);
		txtTenPhuongXa = new JTextField(15);
		txtTenQuanHuyen = new JTextField(15);
		txtTenTinh = new JTextField(15);

		labelMaSo = new JLabel("Unknown");
		labelMaSo.setPreferredSize(txtHoTen.getPreferredSize());
		JLabel labelTieuDeMaSo = new JLabel("Mã số: ");

		JPanel panelForMaSo = new JPanel(new FlowLayout());
		labelTieuDeMaSo.setPreferredSize(new JLabel("Số điện thoại: ").getPreferredSize());
		panelForMaSo.add(labelTieuDeMaSo);
		panelForMaSo.add(labelMaSo);

		this.add(panelForMaSo);

		List<JTextField> textFields = Arrays.asList(new JTextField[] { txtHoTen, txtEmail, txtSoDienThoai, txtSoNha,
				txtTenDuong, txtTenPhuongXa, txtTenQuanHuyen, txtTenTinh });
		List<String> titles = Arrays.asList(new String[] { "Họ tên: ", "Email: ", "Số điện thoại: ", "Số nhà: ",
				"Tên đường: ", "Tên phường: ", "Tên quận: ", "Tên tỉnh: " });

		for (int i = 0; i < textFields.size(); i++) {
			this.add(createPanel(titles.get(i), textFields.get(i)));
		}

		dateModel = new UtilDateModel();
		datePanelImpl = new JDatePanelImpl(dateModel);
		datePickerImpl = new JDatePickerImpl(datePanelImpl, new DateLabelFormatter());
		datePickerImpl.setPreferredSize(new Dimension(txtHoTen.getPreferredSize().width, 30));

		JLabel labelNgaySinh = new JLabel("Ngày sinh: ");
		labelNgaySinh.setPreferredSize(new JLabel("Số điện thoại: ").getPreferredSize());

		JPanel panelNgaySinh = new JPanel(new FlowLayout());
		panelNgaySinh.add(labelNgaySinh);
		panelNgaySinh.add(datePickerImpl);

		this.add(panelNgaySinh);

		JPanel panelForButton = new JPanel(new FlowLayout());
		btnChinhSua = new JButton("Chỉnh sửa");
		try {
			BufferedImage bufferedImage = ImageIO.read(GUI_Login.class.getResource("/icon_libs/edit-icon.png"));
			btnChinhSua = new JButton("Chỉnh sửa", new ImageIcon(bufferedImage));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		btnDoiMatKhau = new JButton("Đổi mật khẩu");
		try {
			BufferedImage bufferedImage = ImageIO.read(GUI_Login.class.getResource("/icon_libs/modify-password-icon.png"));
			btnDoiMatKhau = new JButton("Đổi mật khẩu", new ImageIcon(bufferedImage));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		panelForButton.add(btnChinhSua);
		panelForButton.add(btnDoiMatKhau);

		panelForButton.setPreferredSize(new Dimension(900, 50));

		this.add(panelForButton);
		
		btnChinhSua.addActionListener(this);
		btnDoiMatKhau.addActionListener(this);
		
		hienThiThongTinNhanVien();
	}

	public void hienThiThongTinNhanVien() {
		NhanVien nhanVien = GUI_Login.nhanVien;
		labelMaSo.setText(nhanVien.getMaNV());
		txtHoTen.setText(nhanVien.getHoTen());
		txtEmail.setText(nhanVien.getEmail());
		txtSoDienThoai.setText(nhanVien.getSdt());
		txtSoNha.setText(nhanVien.getDiaChi().getSoNha());
		txtTenDuong.setText(nhanVien.getDiaChi().getTenDuong());
		txtTenPhuongXa.setText(nhanVien.getDiaChi().getTenPhuongXa());
		txtTenQuanHuyen.setText(nhanVien.getDiaChi().getTenQuanHuyen());
		txtTenTinh.setText(nhanVien.getDiaChi().getTenTinhTP());
		dateModel.setDate(nhanVien.getNgaySinh().getYear(), nhanVien.getNgaySinh().getMonthValue() - 1,
				nhanVien.getNgaySinh().getDayOfMonth());
		dateModel.setSelected(true);
		
	}

	public JPanel createPanel(String titleOfLabel, JTextField textField) {
		JLabel label = new JLabel(titleOfLabel);
		JLabel labelModel = new JLabel("Số điện thoại: ");
		label.setPreferredSize(labelModel.getPreferredSize());
		JPanel panelFlow = new JPanel(new FlowLayout(FlowLayout.CENTER));
		panelFlow.add(label);
		panelFlow.add(textField);
		return panelFlow;
	}

	public boolean modifyNhanVienFromTextField() throws RemoteException {
		DiaChi diaChi = new DiaChi(txtSoNha.getText(), txtTenDuong.getText(), txtTenPhuongXa.getText(),
				txtTenQuanHuyen.getText(), txtTenTinh.getText());
		String email = txtEmail.getText();
		String hoTen = txtHoTen.getText();
		LocalDate ngaySinh = LocalDate.of(dateModel.getYear(), dateModel.getMonth() + 1, dateModel.getDay());
		String sdt = txtSoDienThoai.getText();
		GUI_Login.nhanVien.setDiaChi(diaChi);
		GUI_Login.nhanVien.setEmail(email);
		GUI_Login.nhanVien.setHoTen(hoTen);
		GUI_Login.nhanVien.setNgaySinh(ngaySinh);
		GUI_Login.nhanVien.setSdt(sdt);
		return nvService.doiThongTin(GUI_Login.nhanVien);
	}
	
	public boolean isValidateData() {
		String hoTen = txtHoTen.getText().trim();
		String email = txtEmail.getText().trim();
		String soDienThoai = txtSoDienThoai.getText().trim();
		String soNha = txtSoNha.getText().trim();
		String tenDuong = txtTenDuong.getText().trim();
		String tenPhuong = txtTenPhuongXa.getText().trim();
		String tenQuan = txtTenQuanHuyen.getText().trim();
		String tenTinh = txtTenTinh.getText().trim();
		LocalDate ngaySinh = LocalDate.of(dateModel.getYear(), dateModel.getMonth() + 1, dateModel.getDay());

		if (hoTen.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên nhân viên không được rỗng");
			txtHoTen.requestFocus();
			return false;
		} else if (!hoTen.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên nhân viên không được chứa kí tự đặc biệt");
			txtHoTen.requestFocus();
			return false;
		}
		if (email.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Email nhân viên không được rỗng");
			txtEmail.requestFocus();
			return false;
		} else if (!email.matches("^[a-z][a-z0-9_\\.]{1,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$")) {
			JOptionPane.showMessageDialog(this, "Error: Email nhân viên không đúng định dạng");
			txtEmail.requestFocus();
			return false;
		}
		if (soDienThoai.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Số điện thoại nhân viên không được rỗng");
			txtSoDienThoai.requestFocus();
			return false;
		}
		else if (!soDienThoai.matches("^[0-9]{10}$")) {
			JOptionPane.showMessageDialog(this, "Error: Số điện thoại nhân viên chỉ bao gồm 10 chữ số");
			txtSoDienThoai.requestFocus();
			return false;
		}
		if (soNha.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Số nhà không được rỗng");
			txtSoNha.requestFocus();
			return false;
		} else if (!soNha.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Số nhà không được chứa kí tự đặc biệt");
			txtSoNha.requestFocus();
			return false;
		}
		if (tenDuong.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên đường không được rỗng");
			txtTenDuong.requestFocus();
			return false;
		} else if (!tenDuong.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên đường không được chứa kí tự đặc biệt");
			txtTenDuong.requestFocus();
			return false;
		}
		if (tenPhuong.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên phường không được rỗng");
			txtTenPhuongXa.requestFocus();
			return false;
		} else if (!tenPhuong.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên phường không được chứa kí tự đặc biệt");
			txtTenPhuongXa.requestFocus();
			return false;
		}
		if (tenQuan.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên quận không được rỗng");
			txtTenQuanHuyen.requestFocus();
			return false;
		} else if (!tenQuan.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên quận không được chứa kí tự đặc biệt");
			txtTenQuanHuyen.requestFocus();
			return false;
		}
		if (tenTinh.length() == 0) {
			JOptionPane.showMessageDialog(this, "Error: Tên tỉnh không được rỗng");
			txtTenTinh.requestFocus();
			return false;
		} else if (!tenTinh.matches(
				"^[ '.0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$")) {
			JOptionPane.showMessageDialog(this, "Error: Tên tỉnh không được chứa kí tự đặc biệt");
			txtTenTinh.requestFocus();
			return false;
		}
		
		if (LocalDate.now().getYear() - ngaySinh.getYear() < 18) {
			JOptionPane.showMessageDialog(this, "Ngày sinh không hợp lệ, chưa đủ 18 tuổi");
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		new GUI_Login().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		if (o.equals(btnDoiMatKhau)) {
			gui_doiMK=new GUI_DoiMatKhau();
			if(gui_doiMK.isVisible())
				this.setEnabled(false);
		}
		if (o.equals(btnChinhSua)) {
			if (isValidateData()) {
				try {
					if (modifyNhanVienFromTextField()) {
						JOptionPane.showMessageDialog(this, "Chỉnh sửa thông tin thành công!");
					}
					else {
						JOptionPane.showMessageDialog(this, "Chỉnh sửa thông tin không thành công!");
					}
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		gui_doiMK.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                setEnabled(true);
            }
            public void windowClosed(WindowEvent e)
            {
              setEnabled(true);
            }
        });
		
	}
	
	
}
