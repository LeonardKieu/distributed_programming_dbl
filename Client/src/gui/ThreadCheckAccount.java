package gui;

import java.rmi.Naming;
import java.util.concurrent.Callable;

import services.ChiTietChamCongServices;
import services.NhanVienServices;

public class ThreadCheckAccount implements Callable<Boolean> {

	private NhanVienServices nvService;
	private SecurityManager securityManager;
	
	public ThreadCheckAccount() {
		try {
			SecurityManager securityManager = System.getSecurityManager();
			if (securityManager == null) {
				System.setProperty("java.security.policy", "policy\\policyFile.policy");
				securityManager = new SecurityManager();
			}
			nvService = (NhanVienServices) Naming.lookup("rmi://" + GUI_Login.ipAddress + ":1999/nv");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Override
	public Boolean call() throws Exception {
		Thread.sleep(3000);
		if (nvService.getThongTinNhanVien(GUI_Login.nhanVien.getMaNV()) == null) {
			return false;
		}
		return true;
	}

}
