package server_side;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.hibernate.ogm.exception.EntityAlreadyExistsException;

import com.mongodb.MongoBulkWriteException;

import dao.DAOChiTietChamCong;
import dao.DAONgayChamCong;
import dao.DAONhanVien;
import dao.ImportDL;
import entities.NgayChamCong;
import services.implementation.ChiTietChamCongServicesImpl;
import services.implementation.NgayChamCongServicesImpl;
import services.implementation.NhanVienServicesImpl;

public class Server {
	public static void main(String[] args) {
		try {
			SecurityManager securityManager = System.getSecurityManager();
			if (securityManager == null) {
				System.setProperty("java.security.policy", "policy\\policyFile.policy");
				securityManager = new SecurityManager();
			}
			
			// Create stubs
			EntityManager em = Persistence.createEntityManagerFactory("JPA_Services").createEntityManager();
			DAOChiTietChamCong dao_ctcc = new DAOChiTietChamCong(em);
			DAONgayChamCong dao_ncc = new DAONgayChamCong(em);
			DAONhanVien dao_nv = new DAONhanVien(em);
			ChiTietChamCongServicesImpl stubCTCC = new ChiTietChamCongServicesImpl(dao_ctcc, dao_ncc, dao_nv);
			NhanVienServicesImpl stubNV = new NhanVienServicesImpl(dao_nv);
			NgayChamCongServicesImpl stubNgayChamCong = new NgayChamCongServicesImpl(dao_ncc);

			LocateRegistry.createRegistry(1999);
			
//			Rebind the stub
			Naming.rebind("rmi://localhost:1999/ctcc", stubCTCC);
			Naming.rebind("rmi://localhost:1999/nv", stubNV);
			Naming.rebind("rmi://localhost:1999/ncc", stubNgayChamCong);
			
//			Import available data
			try {
				new ImportDL().importData();
				System.out.println("Data is inserted! To create full text indices, use DP_DB and run cmd: db.cacNhanVien.createIndex({ '$**': 'text' })");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Data is available! Server won't insert again!");
			}
			
//			Must have today in cacNgayChamCong
			NgayChamCong obj_ngayChamCong = dao_ncc.findNgayChamCong(LocalDate.now());
			if(obj_ngayChamCong == null) {
				obj_ngayChamCong = dao_ncc.insertToday();
			}
			
//			Some of notifications
			
			System.out.println("Default password for all user is 123456789");
			System.out.println("Server is ready ... ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
