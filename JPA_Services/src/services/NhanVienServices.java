package services;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import entities.NhanVien;

public interface NhanVienServices extends Remote {
	public NhanVien getThongTinNhanVien(String idNhanVien) throws RemoteException;

	public boolean dangNhap(String idNhanVien, String matKhau) throws RemoteException;

	/**
	 * // nếu mã phòng ban rỗng thì get danh sách tất cả nhân viên thuộc mọi phòng
	 * ban. Nếu tiêu chí tìm kiếm rỗng thì tiến hành lấy toàn bộ danh sách
	 * 
	 * @param maPhongBan
	 * @param tieuChiTimKiem
	 * @return
	 * @throws RemoteException
	 */
	public List<NhanVien> getDanhSachNhanVien(String maPhongBan, String search_text) throws RemoteException;
	public List<NhanVien> getDanhSachNhanVien(String maPhongBan) throws RemoteException;

	public boolean resetPassword(String idNhanVien, String matKhauCu, String matKhauMoi) throws RemoteException;

	public boolean doiThongTin(NhanVien nhanVien) throws RemoteException;

	public boolean createNhanVien(NhanVien nhanVien) throws RemoteException;

	public boolean xoaNhanVien(String idNhanVien) throws RemoteException;
}
