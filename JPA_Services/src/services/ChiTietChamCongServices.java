package services;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.List;

import entities.ChiTietChamCong;

public interface ChiTietChamCongServices extends Remote {
	public boolean chamCong(String idNhanVien, LocalDate ngayChamCong) throws RemoteException;

	public List<ChiTietChamCong> getDanhSachChiTietChamCong(String idNhanVien, int thang, int nam) throws RemoteException;
	public int getNgayNghiNhungLam(String idNhanVien, int thang, int nam) throws RemoteException;
	public int getNgayNghiKoHopLe(String idNhanVien, int thang, int nam) throws RemoteException;
}
