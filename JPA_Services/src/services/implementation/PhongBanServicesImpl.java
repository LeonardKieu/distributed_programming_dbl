package services.implementation;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import dao.DAOPhongBan;
import services.PhongBanServices;

public class PhongBanServicesImpl extends UnicastRemoteObject implements PhongBanServices {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOPhongBan daoPhongBan;

	public PhongBanServicesImpl(DAOPhongBan daoPhongBan) throws RemoteException {
		super();
		this.daoPhongBan = daoPhongBan;
	}

}
