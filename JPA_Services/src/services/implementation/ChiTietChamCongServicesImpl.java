package services.implementation;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDate;
import java.util.List;

import dao.DAOChiTietChamCong;
import dao.DAONgayChamCong;
import dao.DAONhanVien;
import entities.ChiTietChamCong;
import entities.NgayChamCong;
import entities.NhanVien;
import services.ChiTietChamCongServices;

public class ChiTietChamCongServicesImpl extends UnicastRemoteObject implements ChiTietChamCongServices {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOChiTietChamCong dao_ctcc;
	private DAONgayChamCong dao_ncc;
	private DAONhanVien dao_nv;

	public ChiTietChamCongServicesImpl(DAOChiTietChamCong dao_ctcc, DAONgayChamCong dao_ncc, DAONhanVien dao_nv) throws RemoteException {
		super();
		this.dao_ctcc = dao_ctcc;
		this.dao_ncc = dao_ncc;
		this.dao_nv = dao_nv;
	}
	/**
	 * (OK)
	 */
	@Override
	public boolean chamCong(String idNhanVien, LocalDate ngayChamCong) throws RemoteException {
		// TODO Auto-generated method stub
		NgayChamCong obj_ngayChamCong = dao_ncc.findNgayChamCong(ngayChamCong);
		if(obj_ngayChamCong == null) {
			obj_ngayChamCong = dao_ncc.insertToday();
		}	
		NhanVien nhanVien = dao_nv.findNhanVien(idNhanVien);
//		Create obj
		ChiTietChamCong ccct = new ChiTietChamCong(obj_ngayChamCong, true, nhanVien);
		return dao_ctcc.insertChiTietChamCong(ccct);
	}
	/**
	 * (OK) Lấy danh sách ChiTietChamCong của nhân viên nào đó trong tháng/năm truyền vào
	 */
	@Override
	public List<ChiTietChamCong> getDanhSachChiTietChamCong(String idNhanVien, int thang, int nam) throws RemoteException {
		// TODO Auto-generated method stub
		return dao_ctcc.getDanhSachCTCC(idNhanVien, thang, nam);
	}
	@Override
	public int getNgayNghiNhungLam(String idNhanVien, int thang, int nam) {
		// TODO Auto-generated method stub
		return dao_ctcc.getNgayNghiNhungLam(idNhanVien, thang, nam);
	}
	@Override
	public int getNgayNghiKoHopLe(String idNhanVien, int thang, int nam) {
		// TODO Auto-generated method stub
		return dao_ctcc.getNgayNghiKoHopLe(idNhanVien, thang, nam);
	}

}
