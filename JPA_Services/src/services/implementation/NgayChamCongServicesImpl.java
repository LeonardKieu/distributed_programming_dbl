package services.implementation;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDate;
import java.util.List;

import dao.DAONgayChamCong;
import entities.NgayChamCong;
import services.NgayChamCongServices;

public class NgayChamCongServicesImpl extends UnicastRemoteObject implements NgayChamCongServices {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAONgayChamCong daoNgayChamCong;

	public NgayChamCongServicesImpl(DAONgayChamCong daoNgayChamCong) throws RemoteException {
		super();
		this.daoNgayChamCong = daoNgayChamCong;
	}

	/**
	 * Dữ liệu phải chuẩn bị sẵn trong collection cacNgayChamCong.
	 * Original idea:
	 * load các ngày chấm công tháng này để chuẩn bị cho nhân viên chấm công, nếu today chưa có trong DB
	 * thì phải tạo và việc quyết định loại ngày phải dựa trên list holiday dương lịch và tính từ âm lịch
	 */

	@Override
	public List<NgayChamCong> loadListNgayChamCongThangNay(LocalDate today) throws RemoteException {
		// TODO Auto-generated method stub
		return daoNgayChamCong.loadListNgayChamCongThangNay(today);
	}

}
