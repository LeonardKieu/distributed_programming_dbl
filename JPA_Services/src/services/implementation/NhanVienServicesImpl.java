package services.implementation;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

import dao.DAONhanVien;
import dao.MD5;
import entities.NhanVien;
import services.NhanVienServices;

public class NhanVienServicesImpl extends UnicastRemoteObject implements NhanVienServices {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAONhanVien daoNhanVien;

	public NhanVienServicesImpl(DAONhanVien daoNhanVien) throws RemoteException {
		super();
		this.daoNhanVien = daoNhanVien;
	}
	/**
	 * (OK)
	 */
	@Override
	public NhanVien getThongTinNhanVien(String idNhanVien) throws RemoteException {
		// TODO Auto-generated method stub
		return daoNhanVien.findNhanVien(idNhanVien);
	}
	/**
	 * (OK)
	 */
	@Override
	public boolean dangNhap(String idNhanVien, String matKhau) throws RemoteException {
		// TODO Auto-generated method stub
		return daoNhanVien.login(idNhanVien, matKhau);
	}
	/**
	 * (OK)
	 */
	@Override
	public List<NhanVien> getDanhSachNhanVien(String maPhongBan, String search_text) throws RemoteException {
		// TODO Auto-generated method stub
		return daoNhanVien.getDanhSachNhanVien(maPhongBan, search_text);
	}
	/**
	 * (OK)
	 */
	@Override
	public boolean resetPassword(String idNhanVien, String matKhauCu, String matKhauMoi) throws RemoteException {
		// TODO Auto-generated method stub
//		Login successfully? => Can change pw
		if(daoNhanVien.login(idNhanVien, matKhauCu)) {
			NhanVien nhanVien = daoNhanVien.findNhanVien(idNhanVien);
			nhanVien.setMatKhau(MD5.getMd5(matKhauMoi));
			return doiThongTin(nhanVien);
		}
		return false;
	}
	/**
	 * (OK)
	 */
	@Override
	public boolean doiThongTin(NhanVien nhanVien) throws RemoteException {
		// TODO Auto-generated method stub
		return daoNhanVien.doiThongTin(nhanVien);
	}
	/**
	 * (OK) Thêm nhân viên. Thành công thì trả true. Thất bại do trùng mã hoặc lí do khác thì false
	 */
	@Override
	public boolean createNhanVien(NhanVien nhanVien) throws RemoteException {
		// TODO Auto-generated method stub
		return daoNhanVien.createNhanVien(nhanVien);
	}
	/**
	 * (OK)
	 */
	@Override
	public boolean xoaNhanVien(String idNhanVien) throws RemoteException {
		// TODO Auto-generated method stub
		return daoNhanVien.xoaNhanVien(idNhanVien);
	}
	@Override
	public List<NhanVien> getDanhSachNhanVien(String maPhongBan) throws RemoteException {
		// TODO Auto-generated method stub
		return daoNhanVien.getDanhSachNhanVien(maPhongBan);
	}

}
