package services.implementation;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import dao.DAODiaChi;
import services.DiaChiServices;

public class DiaChiServicesImpl extends UnicastRemoteObject implements DiaChiServices {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAODiaChi dao_diaChi;

	public DiaChiServicesImpl(DAODiaChi dao_diaChi) throws RemoteException {
		super();
		this.dao_diaChi = dao_diaChi;
	}

}
