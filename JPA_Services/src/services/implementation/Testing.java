package services.implementation;

import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import dao.DAOChiTietChamCong;
import dao.DAONgayChamCong;
import dao.DAONhanVien;
import entities.ChiTietChamCong;
import entities.NhanVien;

public class Testing {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		Test dao chi tiet cham cong
		EntityManager em = Persistence.createEntityManagerFactory("JPA_Services").createEntityManager();
		DAONhanVien dao_nv = new DAONhanVien(em);
		try {
			NhanVienServicesImpl nv_srv = new NhanVienServicesImpl(dao_nv);
			System.out.println(dao_nv.findNhanVien("345"));
			System.out.println("OK");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
