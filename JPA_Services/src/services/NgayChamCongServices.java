package services;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.List;

import entities.NgayChamCong;

public interface NgayChamCongServices extends Remote {
	public List<NgayChamCong> loadListNgayChamCongThangNay(LocalDate today) throws RemoteException;
}
