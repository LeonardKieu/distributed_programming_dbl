package entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class DiaChi implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String soNha;
	private String tenDuong;
	private String tenPhuongXa;
	private String tenQuanHuyen;
	private String tenTinhTP;

	public String getSoNha() {
		return soNha;
	}

	public void setSoNha(String soNha) {
		this.soNha = soNha;
	}

	public String getTenDuong() {
		return tenDuong;
	}

	public void setTenDuong(String tenDuong) {
		this.tenDuong = tenDuong;
	}

	public String getTenPhuongXa() {
		return tenPhuongXa;
	}

	public void setTenPhuongXa(String tenPhuongXa) {
		this.tenPhuongXa = tenPhuongXa;
	}

	public String getTenQuanHuyen() {
		return tenQuanHuyen;
	}

	public void setTenQuanHuyen(String tenQuanHuyen) {
		this.tenQuanHuyen = tenQuanHuyen;
	}

	public String getTenTinhTP() {
		return tenTinhTP;
	}

	public void setTenTinhTP(String tenTinhTP) {
		this.tenTinhTP = tenTinhTP;
	}

	public DiaChi(String soNha, String tenDuong, String tenPhuongXa, String tenQuanHuyen, String tenTinhTP) {
		super();
		this.soNha = soNha;
		this.tenDuong = tenDuong;
		this.tenPhuongXa = tenPhuongXa;
		this.tenQuanHuyen = tenQuanHuyen;
		this.tenTinhTP = tenTinhTP;
	}

	public DiaChi() {
		super();
	}

	@Override
	public String toString() {
		return "DiaChi [soNha=" + soNha + ", tenDuong=" + tenDuong + ", tenPhuongXa=" + tenPhuongXa + ", tenQuanHuyen="
				+ tenQuanHuyen + ", tenTinhTP=" + tenTinhTP + "]";
	}

}
