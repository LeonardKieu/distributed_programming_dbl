package entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cacNgayChamCong")
public class NgayChamCong implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int loaiNgay;
	@Id
	private LocalDate ngay;

	public int getLoaiNgay() {
		return loaiNgay;
	}

	public void setLoaiNgay(int loaiNgay) {
		this.loaiNgay = loaiNgay;
	}

	public LocalDate getNgay() {
		return ngay;
	}

	public void setNgay(LocalDate ngay) {
		this.ngay = ngay;
	}

	public NgayChamCong(int loaiNgay, LocalDate ngay) {
		super();
		this.loaiNgay = loaiNgay;
		this.ngay = ngay;
	}

	public NgayChamCong() {
		super();
	}

	@Override
	public String toString() {
		return "NgayChamCong [loaiNgay=" + loaiNgay + ", ngay=" + ngay + "]";
	}

}
