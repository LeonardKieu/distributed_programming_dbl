package entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@IdClass(PK_ChiTietChamCong.class)
@Table(name = "cacChiTietChamCong")
public class ChiTietChamCong implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ngayChamCong")
	private NgayChamCong ngayChamCong;
	private boolean daChamCong;
	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "maNhanVien")
	private NhanVien nhanVien;

	public boolean isDaChamCong() {
		return daChamCong;
	}

	public void setDaChamCong(boolean daChamCong) {
		this.daChamCong = daChamCong;
	}

	public NhanVien getNhanVien() {
		return nhanVien;
	}

	public void setNhanVien(NhanVien nhanVien) {
		this.nhanVien = nhanVien;
	}

	public ChiTietChamCong(NgayChamCong ngayChamCong, boolean daChamCong, NhanVien nhanVien) {
		super();
		this.ngayChamCong = ngayChamCong;
		this.daChamCong = daChamCong;
		this.nhanVien = nhanVien;
	}

	public ChiTietChamCong() {
		super();
	}

	@Override
	public String toString() {
		return "ChiTietChamCong [ngayChamCong=" + ngayChamCong + ", daChamCong=" + daChamCong + ", nhanVien=" + nhanVien
				+ "]";
	}

	public NgayChamCong getNgayChamCong() {
		return ngayChamCong;
	}

	public void setNgayChamCong(NgayChamCong ngayChamCong) {
		this.ngayChamCong = ngayChamCong;
	}

}
