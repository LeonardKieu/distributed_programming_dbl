package entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cacNhanVien")
public class NhanVien implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int chucVu;
	@Embedded
	private DiaChi diaChi;
	private String email;
	private String hoTen;
	private int luongTheoThang;
	@Id
	@Column(name = "_id")
	private String maNV;
	private LocalDate ngaySinh;
	@ManyToOne
	private PhongBan phongBan;
	private String sdt;
	private String matKhau;

	public int getChucVu() {
		return chucVu;
	}

	public void setChucVu(int chucVu) {
		this.chucVu = chucVu;
	}

	public DiaChi getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(DiaChi diaChi) {
		this.diaChi = diaChi;
	}

	public String getEmail() {
		return email;
	}

	public String getMatKhau() {
		return matKhau;
	}

	public void setMatKhau(String matKhau) {
		this.matKhau = matKhau;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHoTen() {
		return hoTen;
	}

	public void setHoTen(String hoTen) {
		this.hoTen = hoTen;
	}

	public int getLuongTheoThang() {
		return luongTheoThang;
	}

	public void setLuongTheoThang(int luongTheoThang) {
		this.luongTheoThang = luongTheoThang;
	}

	public String getMaNV() {
		return maNV;
	}

	public NhanVien(String hoTen, String maNV) {
		super();
		this.hoTen = hoTen;
		this.maNV = maNV;
	}

	public void setMaNV(String maNV) {
		this.maNV = maNV;
	}

	public LocalDate getNgaySinh() {
		return ngaySinh;
	}

	public void setNgaySinh(LocalDate ngaySinh) {
		this.ngaySinh = ngaySinh;
	}

	public PhongBan getPhongBan() {
		return phongBan;
	}

	public void setPhongBan(PhongBan phongBan) {
		this.phongBan = phongBan;
	}

	public String getSdt() {
		return sdt;
	}

	public void setSdt(String sdt) {
		this.sdt = sdt;
	}

	@Override
	public String toString() {
		return "NhanVien [chucVu=" + chucVu + ", diaChi=" + diaChi + ", email=" + email + ", hoTen=" + hoTen
				+ ", luongTheoThang=" + luongTheoThang + ", maNV=" + maNV + ", ngaySinh=" + ngaySinh + ", phongBan="
				+ phongBan + ", sdt=" + sdt + ", matKhau=" + matKhau + "]";
	}

	public NhanVien() {
		super();
	}

	public NhanVien(int chucVu, DiaChi diaChi, String email, String hoTen, int luongTheoThang, String maNV,
			LocalDate ngaySinh, PhongBan phongBan, String sdt, String matKhau) {
		super();
		this.chucVu = chucVu;
		this.diaChi = diaChi;
		this.email = email;
		this.hoTen = hoTen;
		this.luongTheoThang = luongTheoThang;
		this.maNV = maNV;
		this.ngaySinh = ngaySinh;
		this.phongBan = phongBan;
		this.sdt = sdt;
		this.matKhau = matKhau;
	}

}
