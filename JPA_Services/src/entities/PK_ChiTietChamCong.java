package entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Embeddable;

//@SuppressWarnings("serial")
@Embeddable
public class PK_ChiTietChamCong implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private LocalDate ngayChamCong;
	private String nhanVien;

	public PK_ChiTietChamCong(LocalDate ngayChamCong, String nhanVien) {
		super();
		this.ngayChamCong = ngayChamCong;
		this.nhanVien = nhanVien;
	}

	public String getNhanVien() {
		return nhanVien;
	}

	public void setNhanVien(String nhanVien) {
		this.nhanVien = nhanVien;
	}

	public PK_ChiTietChamCong() {
		super();
	}

	public LocalDate getNgayChamCong() {
		return ngayChamCong;
	}

	public void setNgayChamCong(LocalDate ngayChamCong) {
		this.ngayChamCong = ngayChamCong;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ngayChamCong == null) ? 0 : ngayChamCong.hashCode());
		result = prime * result + ((nhanVien == null) ? 0 : nhanVien.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PK_ChiTietChamCong other = (PK_ChiTietChamCong) obj;
		if (ngayChamCong == null) {
			if (other.ngayChamCong != null)
				return false;
		} else if (!ngayChamCong.equals(other.ngayChamCong))
			return false;
		if (nhanVien == null) {
			if (other.nhanVien != null)
				return false;
		} else if (!nhanVien.equals(other.nhanVien))
			return false;
		return true;
	}

}
