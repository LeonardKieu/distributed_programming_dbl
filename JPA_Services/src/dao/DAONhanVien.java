package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import entities.NhanVien;

public class DAONhanVien {
	private EntityManager em;

	public DAONhanVien(EntityManager em) {
		super();
		this.em = em;
	}

	public NhanVien findNhanVien(String idNhanVien) {
		String query = "db.cacNhanVien.find({'_id': '"+idNhanVien+"'})\r\n" + 
				"";
		List l = em.createNativeQuery(query, NhanVien.class).getResultList();
		if(l.size() == 0)
			return null;
		else
			return (NhanVien) l.get(0);
	}
	public boolean login(String idNhanVien, String matKhau) {
		String query = "db.cacNhanVien.aggregate([\r\n" + 
				"    { '$match': { '_id': '"+idNhanVien+"', 'matKhau': '"+MD5.getMd5(matKhau)+"' } }\r\n" + 
				"])";
		if(em.createNativeQuery(query, NhanVien.class).getResultList().size() == 1)
			return true;
		else
			return false;
	}
	public boolean createNhanVien(NhanVien nhanVien) {
		EntityTransaction t = em.getTransaction();
		try {
			nhanVien.setMatKhau(MD5.getMd5(nhanVien.getMatKhau()));
			t.begin();
			em.persist(nhanVien);
			t.commit();
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	public boolean doiThongTin(NhanVien nhanVien) {
		EntityTransaction t = em.getTransaction();
		try {
			t.begin();
			em.merge(nhanVien);
			t.commit();
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	public boolean xoaNhanVien(String idNhanVien) {
		EntityTransaction t = em.getTransaction();
		try {
			t.begin();
			String query = "db.cacNhanVien.deleteOne({'_id': '"+idNhanVien+"'})";
			int res = em.createNativeQuery(query, NhanVien.class).executeUpdate();
			t.commit();
			return res == 1? true : false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	@SuppressWarnings("unchecked")
	public List<NhanVien> getDanhSachNhanVien(String maPhongBan, String search_text){
		String query = "db.cacNhanVien.find({ '$text': { '$search': '"+search_text+"' }, 'phongBan_maPhongBan': '"+maPhongBan+"' })";
		List<NhanVien> l = em.createNativeQuery(query, NhanVien.class).getResultList();
		if(l.size() > 0)
			return l;
		else
			return new ArrayList<NhanVien>();
	}
	@SuppressWarnings("unchecked")
	public List<NhanVien> getDanhSachNhanVien(String maPhongBan){
		String query = "db.cacNhanVien.find({ 'phongBan_maPhongBan': '"+maPhongBan+"' })";
		List<NhanVien> l = em.createNativeQuery(query, NhanVien.class).getResultList();
		if(l.size() > 0)
			return l;
		else
			return new ArrayList<NhanVien>();
	}
}
