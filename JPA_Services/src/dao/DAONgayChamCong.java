package dao;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import entities.NgayChamCong;

public class DAONgayChamCong {
	private EntityManager em;

	public DAONgayChamCong(EntityManager em) {
		super();
		this.em = em;
	}
	
	public NgayChamCong findNgayChamCong(LocalDate ngayChamCong) {
		return em.find(NgayChamCong.class, ngayChamCong);
	}
	
	@SuppressWarnings("unlikely-arg-type")
	public NgayChamCong insertToday() {
		LocalDate solarToday = LocalDate.now();
//		Detect loaiNgay of today
		int loaiNgay;
		List<Holiday> listSolarHoliday = Arrays.asList(new Holiday[] {
				new Holiday(1, 1), new Holiday(30, 4), new Holiday(1, 5), new Holiday(2, 9)
		});
		List<Holiday> listLunarHoliday = Arrays.asList(new Holiday[] {
				new Holiday(1, 1), new Holiday(2, 1), new Holiday(3, 1), new Holiday(4, 1), new Holiday(5, 1),
				new Holiday(10, 3), 
		});
//		Calc lunarToday
		int[] lunarTodayArr = CalendarCalc.Solar2Lunar(solarToday.getDayOfMonth(), solarToday.getMonthValue(), solarToday.getYear());
		LocalDate lunarToday = LocalDate.of(lunarTodayArr[2], lunarTodayArr[1], lunarTodayArr[0]);
		
		if(listSolarHoliday.contains(new Holiday(solarToday.getDayOfMonth(), solarToday.getMonthValue())) || listLunarHoliday.contains(new Holiday(lunarToday.getDayOfMonth(), lunarToday.getMonthValue())))
//			Holiday 0
			loaiNgay = 0;
		else if(solarToday.getDayOfWeek().equals(DayOfWeek.SATURDAY) || solarToday.getDayOfWeek().equals(DayOfWeek.SUNDAY))
//			Weekends -1
			loaiNgay = -1;
		else
//			Weekday 1
			loaiNgay = 1;
//		Insert
		EntityTransaction t = em.getTransaction();
		try {
			t.begin();
			NgayChamCong newNgayChamCong = new NgayChamCong(loaiNgay, solarToday);
			em.persist(newNgayChamCong);
			t.commit();
			return newNgayChamCong;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<NgayChamCong> loadListNgayChamCongThangNay(LocalDate today){
//		Today exists in coll cacNgayChamCong? 
		
//		Query
		String query2 = "db.cacNgayChamCong.aggregate([\r\n" + 
				"    {\r\n" + 
				"        '$project': {\r\n" + 
				"            '_id': {\r\n" + 
				"                '$dateFromString': {\r\n" + 
				"                    'dateString': '$_id'\r\n" + 
				"                }\r\n" + 
				"            },\r\n" + 
				"            'loaiNgay': 1\r\n" + 
				"        }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"        '$project': {\r\n" + 
				"            'month': { '$month': \"$_id\" },\r\n" + 
				"            'year': { '$year': \"$_id\" },\r\n" + 
				"            'loaiNgay': 1\r\n" + 
				"        }\r\n" + 
				"    },\r\n" + 
				"    { '$match': { 'month': "+today.getMonthValue()+", 'year': "+today.getYear()+" } },\r\n" + 
				"    {\r\n" + 
				"        '$project': {\r\n" + 
				"            '_id': {\r\n" + 
				"                '$dateToString': { 'format': \"%Y-%m-%d\", date: \"$_id\" }\r\n" + 
				"            },\r\n" + 
				"            'loaiNgay': 1\r\n" + 
				"        }\r\n" + 
				"    }\r\n" + 
				"])\r\n" + 
				"";
		return em.createNativeQuery(query2, NgayChamCong.class).getResultList();
	}
}
