package dao;

public class Holiday {
	private int day, month;

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public Holiday(int day, int month) {
		super();
		this.day = day;
		this.month = month;
	}

	@Override
	public String toString() {
		return "SolarHoliday [day=" + day + ", month=" + month + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + day;
		result = prime * result + month;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Holiday other = (Holiday) obj;
		if (day != other.day)
			return false;
		if (month != other.month)
			return false;
		return true;
	}

}
