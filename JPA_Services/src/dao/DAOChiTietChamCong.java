package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import entities.ChiTietChamCong;

public class DAOChiTietChamCong {
	private EntityManager em;

	public DAOChiTietChamCong(EntityManager em) {
		super();
		this.em = em;
	}

	public boolean insertChiTietChamCong(ChiTietChamCong x) {
		EntityTransaction t = em.getTransaction();
		try {
			t.begin();
			em.persist(x);
			t.commit();
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			t.rollback();
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<ChiTietChamCong> getDanhSachCTCC(String idNhanVien, int thang, int nam) {
		String query = "db.cacChiTietChamCong.aggregate([\r\n" + 
				"    { '$match': { '_id.maNhanVien': '"+idNhanVien+"' } },\r\n" + 
				"    {\r\n" + 
				"        '$project': {\r\n" + 
				"            '_id.ngayChamCong': {\r\n" + 
				"                '$dateFromString': {\r\n" + 
				"                    'dateString': '$_id.ngayChamCong'\r\n" + 
				"                }\r\n" + 
				"            },\r\n" + 
				"            '_id.maNhanVien': 1,\r\n" + 
				"            'daChamCong': 1\r\n" + 
				"        }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"        '$project': {\r\n" + 
				"            'month': { '$month': \"$_id.ngayChamCong\" },\r\n" + 
				"            'year': { '$year': \"$_id.ngayChamCong\" },\r\n" + 
				"            'daChamCong': 1\r\n" + 
				"        }\r\n" + 
				"    },\r\n" + 
				"    { '$match': { 'month': "+thang+", 'year': "+nam+" } },\r\n" + 
				"    {\r\n" + 
				"        '$project': {\r\n" + 
				"            '_id.maNhanVien': 1,\r\n" + 
				"            'daChamCong': 1,\r\n" + 
				"            '_id.ngayChamCong': { '$dateToString': { 'format': \"%Y-%m-%d\", date: \"$_id.ngayChamCong\" } }\r\n" + 
				"        }\r\n" + 
				"    }\r\n" + 
				"])      ";
		return em.createNativeQuery(query, ChiTietChamCong.class).getResultList();
	}
	
	public int getNgayNghiNhungLam(String idNhanVien, int thang, int nam) {
		List<ChiTietChamCong> lst_ctcc = getDanhSachCTCC(idNhanVien, thang, nam);
		int res = 0;
		for(ChiTietChamCong ctcc: lst_ctcc) {
			if((ctcc.getNgayChamCong().getLoaiNgay() == 0 || ctcc.getNgayChamCong().getLoaiNgay() == -1) &&
				ctcc.isDaChamCong())
				res++;
		}
		return res;
	}
	
	public int getNgayNghiKoHopLe(String idNhanVien, int thang, int nam) {
		List<ChiTietChamCong> lst_ctcc = getDanhSachCTCC(idNhanVien, thang, nam);
		int res = 0;
		for(ChiTietChamCong ctcc: lst_ctcc) {
			if(ctcc.getNgayChamCong().getLoaiNgay() == 1 && !ctcc.isDaChamCong())
				res++;
		}
		return res;
	}
}
