package dao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.hibernate.HibernateException;

import entities.ChiTietChamCong;
import entities.DiaChi;
import entities.NgayChamCong;
import entities.NhanVien;
import entities.PhongBan;

public class ImportDL {
	public static void main(String[] args) {
		new ImportDL().importData();
	}
	
	public void importData() {
		EntityManager em;
		try {
			NhanVien nv1, nv2, nv3, nv4, nv5, nv6, nv7, nv8, nv9, nv10;

//		Prepare
			em = Persistence.createEntityManagerFactory("JPA_Services").createEntityManager();

			List<NgayChamCong> dsNgayChamCong = new ArrayList<NgayChamCong>();
			// Tạo danh sách ngày chấm công - 20 ngày chấm công - Ngày bth: 1, Lễ: 0, Nghỉ:
			// -1
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 4)));
			dsNgayChamCong.add(new NgayChamCong(-1, LocalDate.of(2019, 10, 5)));
			dsNgayChamCong.add(new NgayChamCong(-1, LocalDate.of(2019, 10, 6)));
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 7)));
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 8)));
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 9)));
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 10)));
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 11)));
			dsNgayChamCong.add(new NgayChamCong(-1, LocalDate.of(2019, 10, 12)));
			dsNgayChamCong.add(new NgayChamCong(-1, LocalDate.of(2019, 10, 13)));
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 14)));
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 15)));
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 16)));
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 17)));
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 18)));
			dsNgayChamCong.add(new NgayChamCong(-1, LocalDate.of(2019, 10, 19)));
			dsNgayChamCong.add(new NgayChamCong(0, LocalDate.of(2019, 10, 20)));
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 21)));
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 22)));
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 23)));
			dsNgayChamCong.add(new NgayChamCong(1, LocalDate.of(2019, 10, 24)));

			List<PhongBan> dsPhongBan = new ArrayList<PhongBan>();
			// Tạo danh sách phòng ban - 5 phòng ban - 1 phòng ban có nhiều NV
			dsPhongBan.add(new PhongBan("A1", "Phong Ke Toan"));
			dsPhongBan.add(new PhongBan("A2", "Phong Kinh Doanh"));
			dsPhongBan.add(new PhongBan("A3", "Phong An Ninh"));
			dsPhongBan.add(new PhongBan("A4", "Phong Quan Ly"));
			dsPhongBan.add(new PhongBan("A5", "Phong Nhan Cong"));

//Create entities
			// NV 1

			DiaChi diachi1 = new DiaChi("34", "Huynh Khuong An", "P5", "Go Vap", "Ho Chi Minh");
			String pw = "123456789";
			String hash = MD5.getMd5(pw);
			nv1 = new NhanVien(1, diachi1, "email@gmail.com", "Kieu Tam Dinh", 2500, "0001", LocalDate.of(1999, 9, 15),
					dsPhongBan.get(1), "0254639847", hash);

			ArrayList<ChiTietChamCong> dsChiTietChamCong1 = new ArrayList<ChiTietChamCong>();

			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(0), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(1), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(2), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(3), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(4), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(5), false, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(6), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(7), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(8), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(9), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(10), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(11), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(12), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(13), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(14), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(15), false, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(16), false, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(17), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(18), true, nv1));
			dsChiTietChamCong1.add(new ChiTietChamCong(dsNgayChamCong.get(19), true, nv1));

			// NV 2
			DiaChi diachi2 = new DiaChi("1", "Nguyen Van Dau", "P1", "Quan 1", "Ho Chi Minh");
			nv2 = new NhanVien(1, diachi1, "lenguyenquanglinh@gmail.com", "Le Nguyen Quang Linh", 3000, "0002",
					LocalDate.of(1999, 11, 25), dsPhongBan.get(1), "0365896547", hash);

			ArrayList<ChiTietChamCong> dsChiTietChamCong2 = new ArrayList<ChiTietChamCong>();

			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(0), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(1), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(2), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(3), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(4), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(5), false, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(6), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(7), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(8), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(9), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(10), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(11), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(12), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(13), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(14), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(15), false, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(16), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(17), true, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(18), false, nv2));
			dsChiTietChamCong2.add(new ChiTietChamCong(dsNgayChamCong.get(19), true, nv2));

			// NV 3

			DiaChi diachi3 = new DiaChi("4", "Tran Thi Nghi", "P2", "Quan 5", "Ho Chi Minh");
			nv3 = new NhanVien(1, diachi1, "doanhbon@gmail.com", "Do Anh Bon", 3200, "0003", LocalDate.of(1999, 1, 14),
					dsPhongBan.get(0), "0548769321", hash);

			ArrayList<ChiTietChamCong> dsChiTietChamCong3 = new ArrayList<ChiTietChamCong>();

			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(0), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(1), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(2), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(3), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(4), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(5), false, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(6), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(7), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(8), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(9), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(10), false, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(11), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(12), false, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(13), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(14), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(15), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(16), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(17), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(18), true, nv3));
			dsChiTietChamCong3.add(new ChiTietChamCong(dsNgayChamCong.get(19), false, nv3));

			// NV 4

			DiaChi diachi4 = new DiaChi("123", "Le Duc Tho", "P4", "Binh Thanh", "Ho Chi Minh");
			nv4 = new NhanVien(1, diachi4, "nguyenthimydung@gmail.com", "Nguyen Thi My Dung", 2100, "0004",
					LocalDate.of(1989, 5, 5), dsPhongBan.get(4), "0325415896", hash);

			ArrayList<ChiTietChamCong> dsChiTietChamCong4 = new ArrayList<ChiTietChamCong>();

			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(0), true, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(1), false, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(2), false, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(3), true, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(4), true, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(5), true, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(6), true, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(7), true, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(8), false, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(9), false, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(10), true, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(11), true, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(12), false, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(13), true, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(14), true, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(15), false, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(16), false, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(17), true, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(18), true, nv4));
			dsChiTietChamCong4.add(new ChiTietChamCong(dsNgayChamCong.get(19), true, nv4));

			// NV 5

			DiaChi diachi5 = new DiaChi("88", "Hoang Van Thu", "P7", "Phu Nhuan", "Ho Chi Minh");
			nv5 = new NhanVien(1, diachi5, "trancamthi@gmail.com", "Tran Cam Thi", 3000, "0005",
					LocalDate.of(1995, 12, 28), dsPhongBan.get(3), "0123546528", hash);

			ArrayList<ChiTietChamCong> dsChiTietChamCong5 = new ArrayList<ChiTietChamCong>();

			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(0), true, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(1), false, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(2), false, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(3), true, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(4), true, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(5), true, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(6), true, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(7), true, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(8), false, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(9), false, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(10), true, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(11), true, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(12), false, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(13), true, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(14), true, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(15), false, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(16), false, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(17), true, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(18), true, nv5));
			dsChiTietChamCong5.add(new ChiTietChamCong(dsNgayChamCong.get(19), true, nv5));

			// NV 6

			DiaChi diachi6 = new DiaChi("11", "Le Hong Phong", "P9", "Binh Duong", "Ho Chi Minh");
			nv6 = new NhanVien(1, diachi6, "nguyenvanhoang@gmail.com", "Nguyen Van Hoang", 1500, "0006",
					LocalDate.of(2001, 11, 11), dsPhongBan.get(1), "0652986321", hash);

			ArrayList<ChiTietChamCong> dsChiTietChamCong6 = new ArrayList<ChiTietChamCong>();

			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(0), true, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(1), false, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(2), false, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(3), true, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(4), true, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(5), false, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(6), true, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(7), true, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(8), false, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(9), false, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(10), true, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(11), true, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(12), false, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(13), true, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(14), true, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(15), true, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(16), false, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(17), true, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(18), false, nv6));
			dsChiTietChamCong6.add(new ChiTietChamCong(dsNgayChamCong.get(19), true, nv6));

			// NV 7

			DiaChi diachi7 = new DiaChi("55", "Ton Duc Thang", "P3", "Quan 3", "Ho Chi Minh");
			nv7 = new NhanVien(1, diachi7, "caoducnhi@gmail.com", "Cao Duc Nhi", 3900, "0007", LocalDate.of(1991, 1, 1),
					dsPhongBan.get(0), "0986587589", hash);

			ArrayList<ChiTietChamCong> dsChiTietChamCong7 = new ArrayList<ChiTietChamCong>();

			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(0), true, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(1), false, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(2), false, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(3), true, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(4), true, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(5), false, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(6), true, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(7), true, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(8), false, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(9), false, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(10), true, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(11), true, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(12), false, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(13), true, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(14), true, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(15), true, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(16), false, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(17), true, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(18), false, nv7));
			dsChiTietChamCong7.add(new ChiTietChamCong(dsNgayChamCong.get(19), true, nv7));

			// NV 8

			DiaChi diachi8 = new DiaChi("4", "Tran Thi Nghi", "P2", "Quan 12", "Ho Chi Minh");
			nv8 = new NhanVien(2, diachi8, "caothihoa@gmail.com", "Cao Thi Hoa", 2550, "0008",
					LocalDate.of(2000, 10, 10), dsPhongBan.get(0), "0111548596", hash);

			ArrayList<ChiTietChamCong> dsChiTietChamCong8 = new ArrayList<ChiTietChamCong>();

			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(0), true, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(1), false, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(2), false, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(3), true, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(4), true, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(5), false, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(6), true, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(7), true, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(8), false, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(9), false, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(10), true, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(11), true, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(12), false, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(13), true, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(14), true, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(15), true, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(16), false, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(17), true, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(18), false, nv8));
			dsChiTietChamCong8.add(new ChiTietChamCong(dsNgayChamCong.get(19), true, nv8));

			// NV 9

			DiaChi diachi9 = new DiaChi("60", "Nguyen Duy Anh", "P4", "Binh Thanh", "Ho Chi Minh");
			nv9 = new NhanVien(3, diachi8, "doananhhung@gmail.com", "Doan Anh Hung", 5000, "0009",
					LocalDate.of(1895, 3, 30), dsPhongBan.get(2), "0542685999", hash);

			ArrayList<ChiTietChamCong> dsChiTietChamCong9 = new ArrayList<ChiTietChamCong>();

			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(0), true, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(1), false, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(2), false, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(3), true, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(4), true, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(5), false, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(6), true, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(7), true, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(8), false, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(9), false, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(10), true, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(11), true, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(12), false, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(13), true, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(14), true, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(15), true, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(16), false, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(17), true, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(18), false, nv9));
			dsChiTietChamCong9.add(new ChiTietChamCong(dsNgayChamCong.get(19), true, nv9));
			// NV 10

			DiaChi diachi10 = new DiaChi("6", "Tran Binh Trong", "P7", "Phu Nhuan", "Ho Chi Minh");
			nv10 = new NhanVien(1, diachi8, "nguyenthicamtu@gmail.com", "Nguyen Thi Cam Tu", 1900, "0010",
					LocalDate.of(1998, 5, 9), dsPhongBan.get(0), "0452187693", hash);

			ArrayList<ChiTietChamCong> dsChiTietChamCong10 = new ArrayList<ChiTietChamCong>();

			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(0), true, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(1), false, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(2), false, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(3), true, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(4), true, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(5), false, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(6), true, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(7), true, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(8), false, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(9), false, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(10), true, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(11), true, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(12), false, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(13), true, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(14), true, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(15), true, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(16), false, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(17), true, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(18), false, nv10));
			dsChiTietChamCong10.add(new ChiTietChamCong(dsNgayChamCong.get(19), true, nv10));

			EntityTransaction tr = em.getTransaction();
			tr.begin();
			for (int i = 0; i < 5; i++)
				em.persist(dsPhongBan.get(i));
			em.persist(nv1);
			em.persist(nv2);
			em.persist(nv3);
			em.persist(nv4);
			em.persist(nv5);
			em.persist(nv6);
			em.persist(nv7);
			em.persist(nv8);
			em.persist(nv9);
			em.persist(nv10);
			for (int i = 0; i < 20; i++) {
				em.persist(dsChiTietChamCong1.get(i));
				em.persist(dsChiTietChamCong2.get(i));
				em.persist(dsChiTietChamCong3.get(i));
				em.persist(dsChiTietChamCong4.get(i));
				em.persist(dsChiTietChamCong5.get(i));
				em.persist(dsChiTietChamCong6.get(i));
				em.persist(dsChiTietChamCong7.get(i));
				em.persist(dsChiTietChamCong8.get(i));
				em.persist(dsChiTietChamCong9.get(i));
				em.persist(dsChiTietChamCong10.get(i));
			}
			tr.commit();
		} catch (HibernateException exception) {
			System.out.println("Problem creating session factory");
			exception.printStackTrace();
		}
	}
}
